/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.collector;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.collector.Collector.CollectorState;
import org.tahomarobotics.robot.lift.Lift;
import org.tahomarobotics.robot.util.UpdaterIF;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Collector extends Subsystem implements UpdaterIF {

	public enum CollectorState {
		COLLECT,
		STOW,
		EJECT,
		MANUAL;
	}

	private static final Logger logger = Logger.getLogger(Collector.class.getSimpleName());
	
	private CollectorState state;
	
	private static final double STALL_TORQUE = 0.15;
	
	private final VictorSPX collectorMotor1 = new VictorSPX(RobotMap.COLLECTOR_LEFT_MOTOR);
	private final VictorSPX collectorMotor2 = new VictorSPX(RobotMap.COLLECTOR_RIGHT_MOTOR);
	
	private final AnalogInput rightLimitSwitch = new AnalogInput(RobotMap.COLLECTOR_RIGHT_LIGHT_SWITCH);
	
	//creates singleton instance (the instance every other command uses)
	private static final Collector instance = new Collector();

	private boolean useSensor = true;
	private boolean testSensor = false;
	
	private static final double LIGHT_LIMIT_VALUE = 1800;
	//constructs the Collector motor when Chassis is called
	private Collector() {
		collectorMotor1.setInverted(true);
		collectorMotor1.setNeutralMode(NeutralMode.Brake);
		
		collectorMotor2.setInverted(false);
		collectorMotor2.setNeutralMode(NeutralMode.Brake);
		collectorMotor2.follow(collectorMotor1);
		
		state = CollectorState.MANUAL;
	}
	
	public static Collector getInstance() {
		return instance;
	}
	
	// the commands you can call with this class
	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new MaintainCube(this));
	}
	
	//lets you set the power
	public void setPower(double motor){
		collectorMotor1.set(ControlMode.PercentOutput, motor);
	}
	
	public boolean isCollected() {
		if(testSensor) {
			logger.log(Level.INFO, "Light Value " + getValueRight());
		}
		if(useSensor) {
			return getValueRight() < LIGHT_LIMIT_VALUE;
		} else {
			return false;	
		}
	}
	
	public double getValueRight() {
		return rightLimitSwitch.getValue();
	}

	public double getStallTorque() {
		return -STALL_TORQUE;
	}
	
	public void setState(CollectorState state) {
		logger.log(Level.INFO, String.format("Collector State: %s", state.toString()));
		this.state = state;
	}
	
	public CollectorState getState() {
		return state;
	}
	
	@Override
	public void init() {
		useSensor = SmartDashboard.putBoolean("Use Sensor", useSensor);
		testSensor = SmartDashboard.putBoolean("Test Sensor", testSensor);
	}

	@Override
	public void update() {
		useSensor = SmartDashboard.getBoolean("Use Sensor", useSensor);
		testSensor = SmartDashboard.getBoolean("Test Sensor", testSensor);
		SmartDashboard.putNumber("Right side", getValueRight());
	}
	
}

class MaintainCube extends Command {

	private Collector collector;
	private OI oi = OI.getInstance();
	
	public MaintainCube(Collector collector) {
		this.collector = collector;
		requires(collector);
	}
	
	@Override
	protected boolean isFinished() {
		double input = (oi.getDriverLeftTrigger() + oi.getManipulatorLeftTrigger()) - (oi.getDriverRightTrigger() + oi.getManipulatorRightTrigger());
		double power = input;
		
		if(Lift.getInstance().getPosition() > 40.0) {
			power *= 0.5;
		}
		
		
		if(collector.getState() == CollectorState.MANUAL && input < 0 && collector.isCollected()){
			oi.startManipulatorLeftRumble(1);
			oi.startManipulatorRightRumble(1);
			oi.startDriverLeftRumble(1);
			oi.startDriverRightRumble(1);
			collector.setState(CollectorState.STOW);
		}
		if(collector.getState() == CollectorState.MANUAL && input == 0) {
			collector.setState(CollectorState.STOW);
		}
		if(collector.getState() == CollectorState.STOW && input != 0) {
			collector.setState(CollectorState.MANUAL);
		}
		if(collector.getState() == CollectorState.STOW) {
			power = collector.getStallTorque();
		}
		
		collector.setPower(-power);
		return false;
	}
	
}