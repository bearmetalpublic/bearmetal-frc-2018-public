/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.collector;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.collector.Collector.CollectorState;

import edu.wpi.first.wpilibj.command.Command;

public class EjectCube extends Command{

	private static final Logger logger = Logger.getLogger(EjectCube.class.getSimpleName());
	
	public enum EjectPower {
		HIGH(-1.0),
		MEDIUM(-0.4),
		LOW(-0.3),
		EXTRA_LOW(-0.2),
		COLLECT(1.0);
		
		public final double power;
		EjectPower(double power){
			this.power = power;
		}
	}
	
	private final Collector collector = Collector.getInstance();
	
	private EjectPower power;
	
	public EjectCube(EjectPower power) {
		requires(collector);
		this.power = power;
		setTimeout(0.5);
	}
	
	@Override
	protected void initialize() {
		collector.setState(CollectorState.EJECT);
	}
	
	@Override
	protected void execute() {
		collector.setPower(power.power);
	}
	
	@Override
	protected boolean isFinished() {
		return isTimedOut();
	}
	
	@Override
	protected void end() {
		collector.setState(CollectorState.MANUAL);		
		logger.log(Level.INFO, String.format("Eject Cube Completed %6.3f", timeSinceInitialized()));

	}
	
	
}
