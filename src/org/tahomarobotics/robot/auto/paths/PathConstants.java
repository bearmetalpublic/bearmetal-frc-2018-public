/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE_RED AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.state.Pose2D;

public class PathConstants {

	public static final double FIELD_WIDTH = 324;
	public static final double FIELD_LENGTH = 648;
	public static final double START_FROM_END_WALL = 19.5;
	public static final double START_FROM_SIDE_WALL = 45;
	public static final double ROBOT_WIDTH = 35.0;
	
	public static final Pose2D RIGHT_POSE = new Pose2D(START_FROM_END_WALL, START_FROM_SIDE_WALL, Math.toRadians(0));
	public static final Pose2D MIDDLE_POSE = new Pose2D(START_FROM_END_WALL, FIELD_WIDTH/2 - ROBOT_WIDTH/2, Math.toRadians(0));
	public static final Pose2D LEFT_POSE = new Pose2D(START_FROM_END_WALL, FIELD_WIDTH - START_FROM_SIDE_WALL, Math.toRadians(0));

	public static final Pose2D RIGHT_POSE_REVERSED = new Pose2D(START_FROM_END_WALL, START_FROM_SIDE_WALL, Math.toRadians(180));
	public static final Pose2D MIDDLE_POSE_REVERSED = new Pose2D(START_FROM_END_WALL, FIELD_WIDTH/2, Math.toRadians(180));
	public static final Pose2D LEFT_POSE_REVERSED = new Pose2D(START_FROM_END_WALL, FIELD_WIDTH - START_FROM_SIDE_WALL, Math.toRadians(180));
	
	public static final double LOOKAHEAD_DISTANCE = 0.6 / 0.0254;
}
