/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.arm.Arm;
import org.tahomarobotics.robot.auto.Collect;
import org.tahomarobotics.robot.auto.CollectPosition;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public abstract class ToSecondCubeForCenterScale extends PathCommand{
	private static double MAX_SPEED = 50;


	public ToSecondCubeForCenterScale(Alliance alliance, RobotPosition position, PathCommand beforeMe) {
		this(alliance, position, false, MAX_SPEED, beforeMe);
	}

	public ToSecondCubeForCenterScale(Alliance alliance, RobotPosition position, boolean test, double speed, PathCommand beforeMe) {
		super(beforeMe.getLastPose().heading + Math.PI, test, alliance, position, speed, beforeMe);
	}

	protected abstract boolean isRightScale();
	
	@Override
	public void createPath() {
		double arcAdj = isRightScale() ? 1.0 : -1.0;
		addArc(Math.toRadians(-83.31 * arcAdj * 0.5), 36.809, MAX_SPEED * 0.3, new CompletionListener() {

			@Override
			public void onCompletion() {
				new Collect().start();
			}
			
		});
		
		addArc(Math.toRadians(-83.31 * arcAdj * 0.5), 36.809, MAX_SPEED * 0.3);
	}
	
	@Override
	public boolean isReversed() {
		return false;
	}
	
	@Override
	protected void initialize() {
		new CollectPosition().start();
		Arm.getInstance().clearForceCubeDetect();
		super.initialize();
	}
}
