/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.arm.Arm;
import org.tahomarobotics.robot.arm.ArmCommand;
import org.tahomarobotics.robot.auto.ReverseScalePosition;
import org.tahomarobotics.robot.collector.EjectCube;
import org.tahomarobotics.robot.collector.EjectCube.EjectPower;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public abstract class CenterToScaleCompanion extends PathCommand {

	private static double MAX_SPEED = 120;

	private final ReverseScalePosition scoreBehind = new ReverseScalePosition();

	public CenterToScaleCompanion(Alliance alliance, RobotPosition position, PathCommand beforeMe) {
		this(alliance, position, false, MAX_SPEED, beforeMe);
	}

	public CenterToScaleCompanion(Alliance alliance, RobotPosition position, boolean test, double speed,
			PathCommand beforeMe) {
		super(beforeMe.getLastPose().heading + Math.PI, test, alliance, position, speed, beforeMe);
		addCompletionCommand(scoreBehind);
	}

	protected abstract boolean isRightScale();

	@Override
	protected void createPath() {
		double leftAdj = isRightScale() ? 1.0 : -1.0;

		addArc(Math.toRadians(137.632 * leftAdj), 30.617, MAX_SPEED * 0.6);

		addLine(26, MAX_SPEED * 0.7);

		addArc(Math.toRadians(42.367 * leftAdj), 224.119, MAX_SPEED * 0.6, new CompletionListener() {

			@Override
			public void onCompletion() {
				scoreBehind.start();
			}

		});

		addLine(24, MAX_SPEED * 0.6);

		addLine(32, MAX_SPEED * 0.4);

		addArc(Math.toRadians(105 * leftAdj), 15.388, MAX_SPEED * 0.3);

		addArc(Math.toRadians(5 * leftAdj), 15.388, MAX_SPEED * 0.3, new CompletionListener() {

			@Override
			public void onCompletion() {
				new EjectCube(EjectPower.LOW).start();
			}

		});

		addLine(4, MAX_SPEED * 0.3);

	}

	@Override
	public boolean isReversed() {
		return true;
	}

	@Override
	protected void initialize() {
		new ArmCommand(ArmCommand.STOW).start();
		super.initialize();
	}

	@Override
	protected void end() {
		Arm.getInstance().clearForceCubeDetect();
		super.end();
	}

}
