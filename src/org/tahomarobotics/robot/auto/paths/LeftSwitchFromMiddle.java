/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.arm.Arm;
import org.tahomarobotics.robot.arm.ArmCommand;
import org.tahomarobotics.robot.collector.EjectCube;
import org.tahomarobotics.robot.collector.EjectCube.EjectPower;
import org.tahomarobotics.robot.lift.LiftCommand;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.command.CommandGroup;

public class LeftSwitchFromMiddle extends PathCommand {

	private static final double MAX_VEL = 120;

	private static final double SPEED = 60;
	
	private static final double RADIUS = 52.601;
	private static final double ANGLE = 64.1;

	public LeftSwitchFromMiddle(Alliance alliance, RobotPosition position) {
		this(alliance, position, false, SPEED);
	}

	public LeftSwitchFromMiddle(Alliance alliance, RobotPosition position, boolean test, double speed) {
		super(test, alliance, position, speed);
	}

	@Override
	protected void createPath() {

		addWaypoint(new Waypoint(PathConstants.MIDDLE_POSE.x, PathConstants.MIDDLE_POSE.y, 0));
		
		addArc(Math.toRadians(ANGLE * 0.2), RADIUS, MAX_VEL * 0.7, new CompletionListener() {

			@Override
			public void onCompletion() {
				new CommandGroup() {
					{
						addParallel(new LiftCommand(15));
						addParallel(new ArmCommand(ArmCommand.STOW));
					}
				}.start();
			}
		});
		
		addArc(Math.toRadians(ANGLE * 0.8), RADIUS, MAX_VEL * 0.7);
		
		addArc(Math.toRadians(-ANGLE * 0.8), RADIUS, MAX_VEL * 0.7, new CompletionListener() {

			@Override
			public void onCompletion() {
				new EjectCube(EjectPower.MEDIUM).start();
			}
			
		});
		
		addArc(Math.toRadians(-ANGLE * 0.2), RADIUS, MAX_VEL * 0.7);
	}

	@Override
	public boolean isReversed() {
		return false;
	}

	@Override
	protected void end() {
		Arm.getInstance().clearForceCubeDetect();
		super.end();
	}
	
	public static void main(String args[]) {
		new LeftSwitchFromMiddle(Alliance.Blue, RobotPosition.RIGHT, true, 0).test();
	}
}
