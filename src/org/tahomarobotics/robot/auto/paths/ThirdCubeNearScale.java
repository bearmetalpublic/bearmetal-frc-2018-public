/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.auto.Collect;
import org.tahomarobotics.robot.auto.CollectPosition;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class ThirdCubeNearScale extends PathCommand{
	private static final double MAX_VEL = 55; // 20 ft/sec

	private static final double SPEED = 120;

	private final CollectPosition moveToBottom = new CollectPosition();
	
	private final Collect collect = new Collect(4.0);
	
	public ThirdCubeNearScale(Alliance alliance, RobotPosition position, PathCommand beforeMe) {
		this(alliance, position, false, SPEED, beforeMe);
	}

	public ThirdCubeNearScale(Alliance alliance, RobotPosition position, boolean test, double speed, PathCommand beforeMe) {
		super(beforeMe.getLastPose().heading + Math.PI, test, alliance, position, speed, beforeMe, 9.0);
		addCompletionCommand(moveToBottom);
		addCompletionCommand(collect);
	}
	@Override
	protected void createPath() {
		
		addArc(Math.toRadians(-79), 40, MAX_VEL * 0.5);
		
		addLine(9, MAX_VEL * 0.4);

	}

	@Override
	protected void initialize() {
		moveToBottom.start();
		collect.start();
		super.initialize();
	}
	
	@Override
	protected void end() {
		collect.cancel();
		super.end();
	}

	@Override
	public boolean isReversed() {
		return false;
	}

}
