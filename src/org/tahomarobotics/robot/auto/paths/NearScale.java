/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE_RED AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.arm.Arm;
import org.tahomarobotics.robot.auto.ReverseScalePosition;
import org.tahomarobotics.robot.collector.EjectCube;
import org.tahomarobotics.robot.collector.EjectCube.EjectPower;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class NearScale extends PathCommand {

	private static final double MAX_VEL = 120; // 10 ft/sec

	private static final double SPEED = 100;
	
	private final ReverseScalePosition scoreBehindCommand = new ReverseScalePosition();
	
	private final EjectCube ejectCube = new EjectCube(EjectPower.MEDIUM);
	
	public NearScale(Alliance alliance, RobotPosition position) {
		this(alliance, position, false, SPEED);
	}

	public NearScale(Alliance alliance, RobotPosition position, boolean test, double speed) {
		super(test, alliance, position, speed);
		
		addCompletionCommand(scoreBehindCommand);
		addCompletionCommand(ejectCube);
	}

	@Override
	protected void createPath() {

		addWaypoint(new Waypoint(PathConstants.RIGHT_POSE.x, PathConstants.RIGHT_POSE.y, Math.PI));
		
		addLine(20, MAX_VEL, new CompletionListener() {
			@Override
			public void onCompletion() {
				scoreBehindCommand.start();
			};
			
		});

		addLine(128, MAX_VEL);
		
		//addArc(Math.toRadians(35), 66, MAX_VEL * 0.4);
		
		addArcToPoint(222.503, 53.941, MAX_VEL * 0.8);
		
		addArcToPoint(262.9, 68, MAX_VEL * 0.6);
		
		addLine(4, MAX_VEL * 0.6, new CompletionListener() {

			@Override
			public void onCompletion() {
				ejectCube.start();
			}
			
		});
	}




	@Override
	public boolean isReversed() {
		return true;
	}

	@Override
	protected void end() {
		super.end();
		Arm.getInstance().clearForceCubeDetect();
	}

	public static void main(String args[]) {
		new NearScale(Alliance.Blue, RobotPosition.RIGHT, true, 0).test();
	}
}

