/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.collector.EjectCube;
import org.tahomarobotics.robot.collector.EjectCube.EjectPower;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class PlaceOnFarSwitch extends PathCommand {

	private static final double MAX_VEL = 50; // 20 ft/sec
	
	private static final double SPEED = 100;
	
	public PlaceOnFarSwitch(Alliance alliance, RobotPosition position, PathCommand beforeMe) {
		this(alliance, position, false, SPEED, beforeMe);
	}

	public PlaceOnFarSwitch(Alliance alliance, RobotPosition position, boolean test, double speed, PathCommand beforeMe) {
		super(beforeMe.getLastPose().heading, test, alliance, position, speed, beforeMe);
	}
	@Override
	protected void createPath() {
		addLine(6, MAX_VEL, new CompletionListener() {

			@Override
			public void onCompletion() {
				new EjectCube(EjectPower.MEDIUM).start();
			};
			
		});
		
		addLine(4, MAX_VEL);
	}

	@Override
	public boolean isReversed() {
		return false;
	}

}
