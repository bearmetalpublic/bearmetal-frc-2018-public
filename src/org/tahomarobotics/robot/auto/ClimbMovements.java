/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.arm.ArmCommand;
import org.tahomarobotics.robot.chassis.MoveStraight;
import org.tahomarobotics.robot.lift.BoostLift;
import org.tahomarobotics.robot.lift.Lift.LiftGear;
import org.tahomarobotics.robot.lift.ShiftLift;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class ClimbMovements extends CommandGroup {

	public ClimbMovements() {
		addSequential(new ShiftLift(LiftGear.HIGH));
		addSequential(new MoveToClimbPosition());
		addSequential(new ArmCommand(ArmCommand.REVERSE));
		addParallel(new BoostLift());
		addSequential(new ShiftLift(LiftGear.LOW));
		addSequential(new MoveStraight(22, 1));
		addSequential(new ArmCommand(ArmCommand.PROTECTED));
		
		
	}
}

