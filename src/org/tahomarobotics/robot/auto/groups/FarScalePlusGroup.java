/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.groups;

import org.tahomarobotics.robot.auto.CollectPosition;
import org.tahomarobotics.robot.auto.paths.FarScale;
import org.tahomarobotics.robot.auto.paths.PlaceSecondOnFarScale;
import org.tahomarobotics.robot.auto.paths.SecondCubeOnScaleFromFar;
import org.tahomarobotics.robot.auto.paths.ThirdCubeOnScaleFromFar;
import org.tahomarobotics.robot.path.PathCommand;
import org.tahomarobotics.robot.path.PathCommand.RobotPosition;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class FarScalePlusGroup extends PathGroup {
	
	public FarScalePlusGroup(Alliance alliance, RobotPosition position){
		PathCommand first = new FarScale(alliance, position);
		addSequentialPath(first);

		PathCommand second = new SecondCubeOnScaleFromFar(alliance, position, first);
		addSequentialPath(second);
		
		PathCommand third = new PlaceSecondOnFarScale(alliance, position, second);
		addSequentialPath(third);
		
		PathCommand fourth = new ThirdCubeOnScaleFromFar(alliance, position, third);
		addSequentialPath(fourth);
		
		addSequential(new CollectPosition());
	}
}
