/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.groups;

import org.tahomarobotics.robot.auto.CollectPosition;
import org.tahomarobotics.robot.auto.NoOp;
import org.tahomarobotics.robot.auto.paths.CenterToScale;
import org.tahomarobotics.robot.auto.paths.CenterToScaleCompanion;
import org.tahomarobotics.robot.auto.paths.CenterToSwitch;
import org.tahomarobotics.robot.auto.paths.CubeZoneToCenter;
import org.tahomarobotics.robot.auto.paths.DriveToCenterCube;
import org.tahomarobotics.robot.auto.paths.LeftSwitchFromMiddle;
import org.tahomarobotics.robot.auto.paths.RightSwitchFromMiddle;
import org.tahomarobotics.robot.auto.paths.SwitchToCenter;
import org.tahomarobotics.robot.auto.paths.ToScaleForCenter;
import org.tahomarobotics.robot.auto.paths.ToSecondCubeForCenterScale;
import org.tahomarobotics.robot.path.PathCommand;
import org.tahomarobotics.robot.path.PathCommand.RobotPosition;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class SwitchFirstThenScaleOrSwitchGroup extends PathGroup {

	public enum Side { LEFT, RIGHT }

	public enum Type { SCALE, SWITCH, COMPANION_SCALE, DOUBLE_SCALE }

	public SwitchFirstThenScaleOrSwitchGroup(Side switchSide, Side scaleSide, Type secondDelivery, Type thirdDelivery, Alliance alliance, RobotPosition position) {



		// deliver first cube switch
		PathCommand first = switchSide == Side.RIGHT ? new RightSwitchFromMiddle(alliance, position) : new LeftSwitchFromMiddle(alliance, position);
		addSequentialPath(first);

		// back up to center line
		PathCommand second = new SwitchToCenter(alliance, position, first) {
			@Override
			protected boolean isRightSwitch() {
				return switchSide == Side.RIGHT;
			}			
		};

		addSequentialPath(second);

		// acquire center cube
		PathCommand third = new DriveToCenterCube(alliance, position, second);
		addSequentialPath(third);

		if(secondDelivery == Type.SWITCH) {
			third = new CubeZoneToCenter(alliance, position, third);
			addSequentialPath(third);
		}


		// deliver second cube to switch or scale
		PathCommand fourth = null;

		if(secondDelivery == Type.SCALE) {
			fourth = new CenterToScale(alliance, position, third) {
				@Override
				protected boolean isRightScale() {
					return scaleSide == Side.RIGHT;
				}
			};
		} else if(secondDelivery == Type.COMPANION_SCALE) {
			fourth = new CenterToScaleCompanion(alliance, position, third) {

				@Override
				protected boolean isRightScale() {
					return scaleSide == Side.RIGHT;
				}

			};
		} else if(secondDelivery == Type.SWITCH) {
			fourth = new CenterToSwitch(alliance, position, third) {

				@Override
				protected boolean isRightSwitch() {
					return switchSide == Side.RIGHT;
				}
			};
		}

		addSequentialPath(fourth);


		if(thirdDelivery != Type.COMPANION_SCALE && secondDelivery != Type.COMPANION_SCALE) {
			PathCommand fifth = thirdDelivery == Type.DOUBLE_SCALE ? new ToSecondCubeForCenterScale(alliance, position, fourth) {

				@Override
				protected boolean isRightScale() {
					return scaleSide == Side.RIGHT;
				}

			} : new NoOp(alliance, position);

			addSequentialPath(fifth);

			PathCommand sixth = thirdDelivery == Type.DOUBLE_SCALE ? new ToScaleForCenter(alliance, position, fifth) {

				@Override
				protected boolean isRightScale() {
					return scaleSide == Side.RIGHT;
				}

			} : new NoOp(alliance, position);

			addSequentialPath(sixth);
			
			addSequential(new CollectPosition());

		}
	}

	public static void main(String[] args) {
		new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.RIGHT, Type.SCALE, Type.SCALE, Alliance.Blue, RobotPosition.RIGHT);
	}
}
