/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.auto.paths.PathConstants;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.PathCommand;

import edu.wpi.first.wpilibj.DriverStation.Alliance;

public class NoOp extends PathCommand {

	private static final double SPEED = 0;
	public NoOp(Alliance alliance, RobotPosition robotPosition) {
		super(false, alliance, robotPosition, SPEED);
		// TODO Auto-generated constructor stub
	}
	
	public NoOp(boolean test, Alliance alliance, RobotPosition robotPosition, double speed) {
		super(test, alliance, robotPosition, speed);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void createPath() {
		addWaypoint(new Waypoint(PathConstants.RIGHT_POSE.x, PathConstants.RIGHT_POSE.y, 0.0));
		addLine(0.0, 0.0);
		
	}

	@Override
	public boolean isReversed() {
		// TODO Auto-generated method stub
		return false;
	}
}
