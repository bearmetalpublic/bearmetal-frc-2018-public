/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto.field;

public class PracticeField {

	// all measurements in inches

	public static double LeftAllianceWallToSwitchEdge = 140;
	public static double BottomWalltoLeftSwitch = 85.25;
	public static double LeftSwitchWidth = 56;
	public static double LeftSwitchToScale = 103.65;
	public static double LeftAllianceWallToScale = 299.65;
	public static double ScaleWidth = 72;
	public static double LeftAllianceWallToLeftRamp = 261.47;

	public static double RightAllianceWallToSwitchEdge = -140;
	public static double BottomWalltoRightSwitch = 85.25;
	public static double RightSwitchWidth = -56;
	public static double RightSwitchToScale = -103.65;
	public static double RightAllianceWallToScale = -299.65;
	public static double RightAllianceWallToLeftRamp = -261.47;
	public static double ScaleLength = 179.95;
	public static double MidlineLength = 141 + 13/16;
	public static double MidlineToScale = 32.735;
	public static double ScaleToSwitch = 65.47;
	public static double MidlineToSwitch = 32.735;

	public static double tolerence = 6;
	public static double cubes = 11;

}
