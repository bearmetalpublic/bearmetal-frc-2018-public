/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;

public class TeleOpDrive extends Command {


	private static final Logger logger = Logger.getLogger(TeleOpDrive.class.getSimpleName());

	private static final int ROTATIONAL_SENSITIVITY = 2;
	private final Chassis chassis = Chassis.getInstance();
	private final OI oi = OI.getInstance();
	
	public TeleOpDrive() {
		requires(chassis);
	}

	protected void initialize() {
		chassis.setGear(Gear.HIGH);

	}

	/**
	 * Returns the forward drive power. It is an average of the left and right
	 * stick.
	 */
	private double getForwardPower() {
		return desensitizeSineBased((oi.getDriverLeftYAxis() + oi.getDriverRightYAxis()) / 2.0);
	}

	/**
	 * Returns the rotational drive power. It is difference of the left and right
	 * stick.
	 */
	private double getRotatePower() {
		return desensitizePowerBased((oi.getDriverLeftYAxis() - oi.getDriverRightYAxis()) / 2.0, ROTATIONAL_SENSITIVITY);
	}

	private double getLeftDrivePower() {
		return getForwardPower() + getRotatePower();
	}

	private double getRightDrivePower() {
		return getForwardPower() - getRotatePower();
	}

	private double desensitizePowerBased(double value, int base) {
		return Math.abs(Math.pow(value, base - 1)) * value;
	}

	
	private double desensitizeSineBased(double value) {
		return (0.5 * (Math.cos(Math.PI*value - Math.PI)) + 0.5) * Math.signum(value);
	}
	
	protected void execute() {
		double l = getLeftDrivePower();
		double r = getRightDrivePower();
		chassis.setPower(l, r);
		if(logger.isLoggable(Level.FINE)) {
			String msg = String.format("Power Left: %6.3f Right: %6.3f", l, r);
			logger.log(Level.FINE, msg); 
		}
		
	}

	@Override
	protected boolean isFinished() {
		return false;
	}
}
