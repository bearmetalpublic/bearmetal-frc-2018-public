/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.RobotConfiguration.RobotConfigurationIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.state.RobotSpeed;
import org.tahomarobotics.robot.util.UpdaterIF;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.Solenoid;
/**
 * Chassis subsystem is responsible for the control and monitoring of the drive base.
 * 
 */
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Chassis extends Subsystem implements UpdaterIF {

	//Enums have valued assigned to them, so we only have to use Gear.Low.value to shift.
	public enum Gear {
		LOW(false),
		HIGH(true);

		public final boolean value;

		Gear(boolean value){
			this.value = value;
		}
	}
	
	private static final RobotConfigurationIF robotConfiguration = RobotConfiguration.getInstance();
	// private to control access
	private final TalonSRX leftMaster = new TalonSRX(RobotMap.LEFT_BACK_MOTOR);
	private final VictorSPX leftSlave1 = new VictorSPX(RobotMap.LEFT_MIDDLE_MOTOR);
	private final VictorSPX leftSlave2 = new VictorSPX(RobotMap.LEFT_FRONT_MOTOR);

	private final TalonSRX rightMaster = new TalonSRX(RobotMap.RIGHT_BACK_MOTOR);
	private final VictorSPX rightSlave1 = new VictorSPX(RobotMap.RIGHT_MIDDLE_MOTOR);
	private final VictorSPX rightSlave2 = new VictorSPX(RobotMap.RIGHT_FRONT_MOTOR);
	
	private final Solenoid gearShifter = new Solenoid(RobotMap.PCM_MODULE, RobotMap.DRIVE_SOLENOID);

	private static final double WHEEL_DIA = robotConfiguration.getWheelDia(); // inches
	private static final double WHEEL_CIRCUM = WHEEL_DIA * Math.PI; 
	private static final double INCHES_PER_PULSE = WHEEL_CIRCUM / 4096.0; //inches per tic(4096 is the amount of tics per revolution within documentation)
	private static final double WHEELBASE_WIDTH = 24.375; //25 + 3/8; First is in inches, second is in inches
	
	private static final double _100MS_PER_SECOND = 10;
	
	private static final double WHEEL_SCRUB = 0.80;

	private static final double LEFT_KP = 0.4;
	private static final double LEFT_KI = 0.0;
	private static final double LEFT_KD = 0.6;
	private static final double LEFT_KFFV = robotConfiguration.getRightKFFV();

	private static final double RIGHT_KP = 0.7; //0.7
	private static final double RIGHT_KI = 0.0;
	private static final double RIGHT_KD = 0.4; //0.4
	private static final double RIGHT_KFFV = robotConfiguration.getRightKFFV();
	private Gear gearState;
	
	private double positionError;
	
	// singleton instance
	private static Chassis instance = new Chassis();

	private Chassis() {
		//Set the talons to receive feedback from encoders attached to them
		leftMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);
		rightMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);

		//Invert the left side motors for use later
		leftMaster.setInverted(true);
		leftSlave1.setInverted(true);
		leftSlave2.setInverted(true);
		leftMaster.config_kP(0, LEFT_KP, 0);
		leftMaster.config_kI(0, LEFT_KI, 0);
		leftMaster.config_kD(0, LEFT_KD, 0);
		leftMaster.config_kF(0, LEFT_KFFV, 0);
		//Set the 4 VictorSPX to be slaves
		leftSlave1.follow(leftMaster);
		leftSlave2.follow(leftMaster);
		
		rightSlave1.follow(rightMaster);
		rightSlave2.follow(rightMaster);	

		rightMaster.config_kP(0, RIGHT_KP, 0);
		rightMaster.config_kI(0, RIGHT_KI, 0);
		rightMaster.config_kD(0, RIGHT_KD, 0);
		rightMaster.config_kF(0, RIGHT_KFFV, 0);
	}
	
	//returns distance in inches for left side
	private double getLeftEncoderVelocity(){
		return toInchesPerSecond(leftMaster.getSelectedSensorVelocity(0));
	}
	
	//returns distance in inches for left side
	private double getRightEncoderVelocity(){
		return toInchesPerSecond(rightMaster.getSelectedSensorVelocity(0));
	}
	
	/**
	 * 
	 * @param Pulses Per 100 Ms
	 * @return Inches Per Second
	 */
	private double toInchesPerSecond(double pulsesPer100Ms) {
		return pulsesPer100Ms * INCHES_PER_PULSE * _100MS_PER_SECOND;
	}
	/**
	 * 
	 * @param velocity / inches per second
	 * @return ticks / 100ms
	 */
	private double toTicksPer100ms(double velocity) {
		return velocity / INCHES_PER_PULSE / _100MS_PER_SECOND;
	}
	
	//sets power in a range from 1 to -1 
	public void setPower(double left, double right) {
		leftMaster.set(ControlMode.PercentOutput, left);
		rightMaster.set(ControlMode.PercentOutput, right);
	}

	//applys the gear that you want to the shifters
	public void setGear(Gear gear) {
		gearShifter.set(gear.value);
		gearState = gear;
	}
	
	//lets other classes know the current gear state
	public Gear getGearState() {
		return gearState;
	}

	/**
	 * Return the Chassis singleton instance
	 */
	public static Chassis getInstance() {
		return instance;
	}

	/**
	 * Installs the default command for this chassis
	 */
	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new TeleOpDrive());
	}

	/**
	 * 
	 * @return a robotSpeed with its forward and rotational
	 */
	public RobotSpeed getOdometrySpeed() {
		double forwardSpeed = (getLeftEncoderVelocity() + getRightEncoderVelocity()) / 2.0;
		double rotationalSpeed = (getRightEncoderVelocity() - getLeftEncoderVelocity()) / 2.0 / WHEELBASE_WIDTH;
		
		return new RobotSpeed(forwardSpeed, rotationalSpeed);
	}
	private double wantedForwardSpeed;
	//applys the current speeds to smart dashboard
	@Override
	public void update() {
		SmartDashboard.putNumber("Chassis Left Distance", leftMaster.getSelectedSensorPosition(0) * INCHES_PER_PULSE);
		SmartDashboard.putNumber("Chassis Right Distance", rightMaster.getSelectedSensorPosition(0) * INCHES_PER_PULSE);
		SmartDashboard.putNumber("Chassis forward Velocity", (getLeftEncoderVelocity() + getRightEncoderVelocity()) / 2);
		SmartDashboard.putNumber("Chassis Wanted Velocity", wantedForwardSpeed == Double.NaN ? 0.0 : wantedForwardSpeed);
		SmartDashboard.putNumber("Error in Position Chassis", positionError);
	}

	public void setWantedVelocity(double velocity) {
		wantedForwardSpeed = velocity;
	}
	
	@Override
	public void init() {
		
	}

	public void setSpeed(RobotSpeed speedCommand) {
		double delta_v = WHEELBASE_WIDTH * speedCommand.rotational / (2.0 * WHEEL_SCRUB);
		double leftSpeed = speedCommand.forward - delta_v;
		double rightSpeed = speedCommand.forward + delta_v;
		SmartDashboard.putNumber("Left Wanted Velocity", leftSpeed);
		SmartDashboard.putNumber("Right Wanted Velocity", rightSpeed);
		leftMaster.set(ControlMode.Velocity, toTicksPer100ms(leftSpeed));
		rightMaster.set(ControlMode.Velocity, toTicksPer100ms(rightSpeed));
		
	}

	public void setPositionError(double positionError) {
		this.positionError = positionError;
	}

}