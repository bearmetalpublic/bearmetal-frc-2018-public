/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.lift.test;

import java.util.HashMap;
import java.util.Map;

import org.tahomarobotics.robot.motion.MotionState;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestLiftDeceleration extends Application {
	
	private final LiftModel model = new LiftModel();
	
	private final Map<String, XYChart.Series<Number,Number>> series = new HashMap<>();
	
	private void addData(String name, double time, double value) {
		XYChart.Series<Number,Number> data = series.get(name);
		if (data == null) {
			data = new XYChart.Series<>();
			data.setName(name);
			series.put(name, data);
		}
		data.getData().add(new XYChart.Data<Number,Number>(time, value));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		

		
		double yMax = 0;
		double yMin = 0;
		final MotionState setpoint = new MotionState();
		final MotionState currentState = new MotionState();
		
		
		model.position = 0 * 0.0254;
		model.velocity = 0;
		double time = 0;
		double input = 12;
		double max = 62.5 ;
		for (int i = 0; i < 2; i++) {
		
			model.velocity = 0.0011;
			for (; time < 2.0; time += 0.001) {
			
				
				if (Math.abs(model.velocity) < 0.001) {
					break;
				}
				currentState.position = model.position / 0.0254;
				yMax = Math.max(yMax, setpoint.position);
				yMax = Math.max(yMax, currentState.position);
				yMin = Math.min(yMin, setpoint.position);
				yMin = Math.min(yMin, currentState.position);
				currentState.velocity = model.velocity / 0.0254;
				yMax = Math.max(yMax, setpoint.velocity);
				yMax = Math.max(yMax, currentState.velocity);
				yMin = Math.min(yMin, setpoint.velocity);
				yMin = Math.min(yMin, currentState.velocity);
				currentState.acceleration = model.acceleration / 0.0254;				
				
				addData("Voltage", time, model.voltage);
				addData("Position", time, currentState.position);
				addData("Velocity", time, currentState.velocity);
				addData("Accel", time, currentState.acceleration);
				
				double command = limitVoltage(input, currentState.position, currentState.velocity);
//				double limit = 0;
//				double cmdLimit = 0;
//				double command = input;
//				if (currentState.velocity < 0 && currentState.position > 0) {
//					limit = -maxVelocity(currentState.position, 400);
//					cmdLimit = LiftModel.kFFP + LiftModel.kFFV * limit + LiftModel.kFFA * 400;
//					command = Math.max(cmdLimit, input);
//				} if (currentState.velocity > 0 && currentState.position < max) {
//					limit = maxVelocity(max - currentState.position, 400);
//					cmdLimit = LiftModel.kFFP + LiftModel.kFFV * limit + LiftModel.kFFA * -400;
//					command = Math.min(cmdLimit, input);					
//				}
//				
				System.out.format("%6.3f %6.3f %6.3f %6.3f \n", 
						currentState.position, currentState.velocity, currentState.acceleration, command);
				model.update(time, command);
			}
			input = -12;
			model.position = max * 0.0254;
		}

		double xMin = 0;
		double xMax = time;
		yMax *= 1.1;
		yMin *= 1.1;
		
		primaryStage.setTitle("Lift Motion");
        final NumberAxis xAxis = new NumberAxis(xMin, xMax, 0.1);
        final NumberAxis yAxis = new NumberAxis(yMin, yMax, 5);        
        final LineChart<Number,Number> sc = new LineChart<>(xAxis,yAxis);
        xAxis.setLabel("Time (sec)");                
        yAxis.setLabel("Velocity (inch/sec)");
        sc.setTitle("Lift Motion");

        sc.setPrefSize(1024, 768);
        sc.getData().addAll(series.values());
        Scene scene  = new Scene(new Group());
        final VBox vbox = new VBox();
        final HBox hbox = new HBox();
               
        hbox.setSpacing(10);
        
        vbox.getChildren().addAll(sc);
        hbox.setPadding(new Insets(10, 10, 10, 50));
        
        ((Group)scene.getRoot()).getChildren().add(vbox);
        
        scene.getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        
        primaryStage.setScene(scene);
        primaryStage.show();
		
	}
 
	private static final double MAX_ACCEL = 400;
	
	private double limitVoltage(double input, double position, double velocity) {
		
		double limit, cmdLimit, cmd;
		
		if (velocity < 0) {
			limit = -Math.sqrt(2 * MAX_ACCEL * Math.max(position,0));
			cmdLimit = LiftModel.kFFP + LiftModel.kFFV  * limit + LiftModel.kFFA * MAX_ACCEL;
			cmd = Math.max(input, cmdLimit);
		} else {
			position = 62.5 - position;
			limit = Math.sqrt(2 * MAX_ACCEL * Math.max(position,0));
			cmdLimit = LiftModel.kFFP + LiftModel.kFFV * limit - LiftModel.kFFA * MAX_ACCEL;
			cmd = Math.min(input, cmdLimit);
		}
		
		return cmd;
	}
	
	public static void main(String[] args) {
        launch(args);
    }

}
