/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.lift;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;
import org.tahomarobotics.robot.motion.MotionProfile.MotionProfileException;

import edu.wpi.first.wpilibj.command.Command;

public class LiftCommand extends Command {

	private static final Logger logger = Logger.getLogger(LiftCommand.class.getSimpleName());

	public static final double COLLECT = 0.0;
	public static final double SWITCH = 26.0;
	public static final double MAX_SCALE = 61.0;
	public static final double REVERSE_SCALE = 62.25;
	public static final double CLIMB = 61.0;
	
	private static final double MAX_VELOCITY = 50;
	private static final double MAX_ACCLERATION = 170;
	private static final double MAX_JERK = 500;

	private Lift lift = Lift.getInstance();
	private MotionProfile profile;
	private double position;
	private final double speed;
	
	public LiftCommand(double position) {
		this.position = position;
		speed = MAX_VELOCITY;
	}
	
	public LiftCommand(double position, double speed) {
		this.position = position;
		this.speed = speed;
	}
	private boolean failed;
	protected void initialize(double position) {

		double currentPosition = lift.getPosition();
		Conditions constraint = new Conditions(
				Profile.BetterSCurve, 
				currentPosition, // start position
				position, // desired position
				0, // start velocity
				0, // end velocity
				speed,
				MAX_ACCLERATION, 
				MAX_JERK);
		
		try {
			failed = false;
			profile = new MotionProfile(0, constraint);
			lift.startMotion(profile);
		} catch (MotionProfileException e) {
			logger.log(Level.SEVERE, String.format("Lift Profile failed with %6.3f %6.3f", Math.toDegrees(currentPosition), Math.toDegrees(position)), e);
			failed = true;

		}
		
	}
	
	@Override
	protected void initialize() {
		initialize(position);
	}
	
	@Override
	protected boolean isFinished() {
		if (failed) {
			return true;
		}
		boolean finished = lift.isMotionComplete();

		if (finished) {
			logger.log(Level.INFO, "Lift Command expected " + profile.getEndPosition() + " actual " + lift.getPosition() + (lift.isTimedOut() ? " Timedout" : ""));
		}
		return finished;
	}
	
	@Override
	protected void end() {
		logger.log(Level.INFO, String.format("Lift Command Completed %6.3f", timeSinceInitialized()));
		lift.endMotion();
	}
}
