/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.lift;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.lift.Lift.LiftGear;

import edu.wpi.first.wpilibj.command.Command;

public class ShiftLift extends Command {

	private static final Logger logger = Logger.getLogger(ShiftLift.class.getSimpleName());
	
	private final Lift lift = Lift.getInstance();

	private final LiftGear gear;

	public ShiftLift() {
		this(null);
	}

	public ShiftLift(LiftGear gear) {
		this.gear = gear;

	}

	@Override
	protected void initialize() {
		if(gear == null) {
			lift.setShift(lift.getShiftState() == LiftGear.LOW ? LiftGear.HIGH : LiftGear.LOW);
		}else {
			lift.setShift(gear);
		}
		logger.log(Level.INFO, "Shifted to " + lift.getShiftState());
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

}
