/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.lift;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.RobotConfiguration.RobotConfigurationIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.lift.test.LiftModel;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileController;
import org.tahomarobotics.robot.motion.MotionProfileController.ControllerParameters;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.LockOut;
import org.tahomarobotics.robot.util.LockOut.LockOutIF;
import org.tahomarobotics.robot.util.UpdaterIF;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class Lift extends Subsystem implements UpdaterIF, LockOutIF {

	private static final RobotConfigurationIF robotConfiguration = RobotConfiguration.getInstance();
	private static final Logger logger = Logger.getLogger(Lift.class.getSimpleName());

	private final LockOut lockOut = LockOut.getInstance();

	private final TalonSRX masterMotor = new TalonSRX(RobotMap.LIFT_TOP_MOTOR);
	private final VictorSPX slaveMotor1 = new VictorSPX(RobotMap.LIFT_MIDDLE_MOTOR);
	private final VictorSPX slaveMotor2 = new VictorSPX	(RobotMap.LIFT_BOTTOM_MOTOR);

	private final Solenoid gearShifter = new Solenoid(RobotMap.PCM_MODULE, RobotMap.LIFT_SOLENOID);

	private static final double MOTION_kP  = robotConfiguration.getLiftKP();
	private static final double MOTION_kV  = robotConfiguration.getLiftKV();
	private static final double MOTION_kI  = robotConfiguration.getLiftKI();

	private final static double STALL_PERCENTAGE_HIGH = RobotConfiguration.getInstance().getLiftHighStall();
	private final static double STALL_PERCENTAGE_LOW = RobotConfiguration.getInstance().getLiftLowStall();

	private final static double STATIC_FRICTION_POWER = robotConfiguration.getLiftStaticFriction();

	private final ControllerParameters highGearCntrlParam = new ControllerParameters(
			MOTION_kP, MOTION_kV, MOTION_kI, 
			LiftModel.kFFV_HIGH * robotConfiguration.getLiftVelocityFudgeFactor(), 
			LiftModel.kFFA_HIGH * robotConfiguration.getLiftAccelerationFudgeFactor(), 0.25);

	private final ControllerParameters lowGearCntrlParam = new ControllerParameters(
			MOTION_kP, MOTION_kV, MOTION_kI, LiftModel.kFFV_LOW, LiftModel.kFFA_LOW, 0.25);


	private final MotionProfileController controller = new MotionProfileController();

	private static Lift instance = new Lift();

	private final MotionState currentState = new MotionState();

	private final MotionState setPoint = new MotionState();

	private final static double INCHES_PER_ROTATION = 9 * 2.0;
	private final static double INCHES_PER_PULSE = INCHES_PER_ROTATION / 4096.0;

	private final static double _100MS_PER_SECOND = 10;

	private static final double TOP_LIFT_POSITION = 62.25;
	private static final double BOTTOM_LIFT_POSITION = 0.8;
	private static final double ZEROING_POWER = -0.2;
	private static final int ZEROED_COUNT_THRESHOLD = 10;
	private static final double LIFT_INPUT_DEADBAND = 0.2;
	private static final double LIFT_DERATE = 0.6;
	
	private static final double LIFT_CONTROL_PERIOD = 0.005;

	private static final double MICROSEC_PER_SECOND = 1.0e6;

	private static final double MOTION_TIMEOUT_THRESHOLD = 0.25;

	private volatile boolean debug = false;

	private static final double SAFE_HEIGHT = 59;

	private static double boostEndTime;
	private static final double BOOST_POWER = 0.3;
	private static final double BOOST_DURATION = 0.5;
	
	private LiftGear gearState;
	public enum LiftGear {
		HIGH(false),
		LOW(true);

		public final boolean value;

		LiftGear(boolean value){
			this.value = value;
		}
	}

	private Lift() {

		lockOut.setLift(this);

		masterMotor.setSensorPhase(false);
		masterMotor.setSelectedSensorPosition(0, 0, 0);

		masterMotor.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_5Ms, 0);
		masterMotor.configVelocityMeasurementWindow(1, 0);
		masterMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 5, 0);

		setCurrentLimit(false);
		
		masterMotor.setNeutralMode(NeutralMode.Brake);
		slaveMotor1.setNeutralMode(NeutralMode.Brake);
		slaveMotor2.setNeutralMode(NeutralMode.Brake);

		slaveMotor1.follow(masterMotor);
		slaveMotor2.follow(masterMotor);

		masterMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);
		masterMotor.setSelectedSensorPosition(0, 0, 0);

	}

	public void setCurrentLimit(boolean isClimbing) {
		
		int peak = isClimbing ? 50 : 25;
		int cont = isClimbing ? 30 : 15;
		
		/**
		 * Once peak current has been exceeded for the duration, the current is limited down
		 * to the continuous current limit.  Current is specified in Amps and duration
		 * is specified in milliseconds.
		 */
		masterMotor.configPeakCurrentLimit(peak, 0);
		masterMotor.configPeakCurrentDuration(200, 0); 
		masterMotor.configContinuousCurrentLimit(cont, 0); 
		masterMotor.enableCurrentLimit(true);
		
	}
	
	public enum LiftState {
		Init,
		Zeroing,
		Hold,
		Motion,
		Manual,
		BoostStart,
		Boost,
		DPAD_MOVEMENT
	}

	private volatile LiftState liftState = LiftState.Init;

	private double prevTime = Timer.getFPGATimestamp();

	private volatile int loopTimeUsed = 0;
	private volatile int loopPeriod = 0;

	private int zeroingCount = 0;

	private MotionProfile profile = null;

	private volatile boolean timedOut;

	private Notifier m_loop = new Notifier(new Runnable() {
		@Override
		public void run() {
			runControlLoop();
		}
	});


	/**
	 * Main lift control loop, all lift function handle with loop and state machine
	 */
	private void runControlLoop() {

		double time = Timer.getFPGATimestamp();

		switch (liftState) {

		// Initialization - start zeroing
		case Init:
			if (!DriverStation.getInstance().isEnabled()) {
				break;
			}
			logger.log(Level.INFO, "Zeroing Start");
			setPosition(1000);
			zeroingCount = 0;
			setShift(LiftGear.HIGH);
			setPower(ZEROING_POWER);
			liftState = LiftState.Zeroing;
			// fall through

			// Zeroing - drive down the lift until position doesn't go negative any more
		case Zeroing:
			boolean isZeroed = (getPosition() == 0) && (zeroingCount++ > ZEROED_COUNT_THRESHOLD);
			setPosition(0);
			if (isZeroed) {
				logger.log(Level.INFO, "Zeroing Complete");
				setPower(0);
				initializeHold(time);
			}
			break;

			// Hold - turn one stall current to maintain position
		case Hold:
			holdControl(time);
			if (manualControl()) {
				liftState = LiftState.Manual;
			}
			
			if (dpadControl(time)) {
				liftState = LiftState.DPAD_MOVEMENT;
			}
			break;

		case Motion:
			motionControl(time);
			break;

		case Manual:
			if (!manualControl()) {
				logger.log(Level.INFO, String.format("Manual Complete -> %4.1f", getPosition()));
				initializeHold(time);
			}
			break;
		
		case DPAD_MOVEMENT:
			if(liftState == LiftState.Motion) {
				logger.log(Level.INFO, "Automated Movements occuring");
				break;
			}
			
			if(!dpadControl(time)) {
				logger.log(Level.INFO, String.format("D-PAD Manual Complete -> %4.1f", getPosition()));
				initializeHold(time);
			}
			
			break;
		case BoostStart:
			boostEndTime = time + BOOST_DURATION;
			liftState = LiftState.Boost;
			
		case Boost:
			if(time > boostEndTime) {
				logger.log(Level.INFO, String.format("Boost Complete -> %4.1f", getPosition()));
				initializeHold(time);
			} else {
				setPower(BOOST_POWER);
			}
			break;
		}

		double end = Timer.getFPGATimestamp();
		loopTimeUsed = (int) ((end - time) * MICROSEC_PER_SECOND);
		loopPeriod = (int) ((time - prevTime) * MICROSEC_PER_SECOND);
		prevTime = time;
	}

	private void initializeHold(double time) {
		setPoint.position = getPosition();
		setPoint.velocity = 0;
		setPoint.acceleration = 0;
		setPoint.jerk = 0;
		liftState = LiftState.Hold;
		holdControl(time);
	}

	public boolean isBoostComplete() {
		return liftState != LiftState.Boost && liftState != LiftState.BoostStart;
	}
	
	public void setBoostStart() {
		liftState = LiftState.BoostStart;
		
	}
	/** 
	 * checks to see if there is a manual input form controller
	 * 
	 * @return true if input is greater than zero (larger dead band applied)
	 */
	private boolean manualControl() {
		double liftInput = getLiftInput();
		boolean hasInput = Math.abs(liftInput) > 0;
		if (hasInput) {

			double position = getPosition();
			if (position > TOP_LIFT_POSITION  && liftInput > 0) {
				hasInput = false;
			} else if (position < BOTTOM_LIFT_POSITION && liftInput < 0) {
				hasInput = false;
			}
		}

		if (hasInput) {
			liftInput = Math.signum(liftInput) * Math.min(Math.abs(liftInput), LIFT_DERATE);
			double modifiedLiftInput = limitVoltage(liftInput, getPosition(), getVelocity());
			//System.out.format("%6.3f, %6.3f, %6.3f, %6.3f\n", modifiedLiftInput, liftInput, getPosition(), getVelocity());
			setPower(getHoldPower() + modifiedLiftInput);
		}
		return hasInput;
	}
	
	private boolean dpadControl(double time) {
		switch(OI.getInstance().getDriverPOV()) {
		case 0:
			setPower(getHoldPower() + limitVoltage(0.2, getPosition(), getVelocity()));
			break;
		case 180:
			setPower(getHoldPower() + limitVoltage(-0.2, getPosition(), getVelocity()));
			break;
		default:
			return false;
		}
		
		return true;
	}
	
	private static final double MAX_ACCEL = 40;
	
	private double limitVoltage(double input, double position, double velocity) {
		
		double limit, cmdLimit, cmd;
		input *= 12;
		if (velocity < 0) {
			limit = -Math.sqrt(2 * MAX_ACCEL * Math.max(position,0));
			cmdLimit = LiftModel.kFFV  * limit + LiftModel.kFFA * MAX_ACCEL;
			cmd = Math.max(input, cmdLimit);
		} else {
			position = TOP_LIFT_POSITION - position;
			limit = Math.sqrt(2 * MAX_ACCEL * Math.max(position,0));
			cmdLimit = LiftModel.kFFV * limit - LiftModel.kFFA * MAX_ACCEL;
			cmd = Math.min(input, cmdLimit);
		}
		
		limit = cmd/12;
		
		double sign = Math.signum(limit);
		double min = input < 0 ? 0.05 : 0.25;
		limit = sign * Math.max(Math.abs(limit), Math.min(input, min));
		
		return limit;
	}
	/**
	 * Get the lift input from OI and applies the dead band
	 * 
	 * @return the input with dead band applied
	 */
	private double getLiftInput() {
		return OI.applyDeadBand(OI.getInstance().getManipulatorRightYAxis(), LIFT_INPUT_DEADBAND);
	}

	private double motionStartTime = 0;


	public boolean startMotion(MotionProfile profile) {
		if (liftState != LiftState.Hold) {
			logger.log(Level.SEVERE, "Motion started in incorrect State: " + liftState);
			return false;
		}

		this.profile = profile;
		motionStartTime = Timer.getFPGATimestamp();


		liftState = LiftState.Motion;
		logIndex = 0;
		stictionComplete = false;
		controller.reset();
		motionControl(motionStartTime);

		return true;
	}

	private double getHoldPower() {
		return getShiftState() == LiftGear.HIGH ? STALL_PERCENTAGE_HIGH : STALL_PERCENTAGE_LOW;
	}

	private void holdControl(double time) {
		if (getPosition() > BOTTOM_LIFT_POSITION) {
			setPower(getHoldPower());
		} else {
			setPower(0);
		}
		
	}

	private final double logValues[] = new double[4000];
	private int logIndex;
	private boolean stictionComplete;
	private boolean motionControl(double time) {

		// subtract motion start time
		double motionTime = time - motionStartTime;		
		boolean profileComplete = motionTime > profile.getEndTime();
		timedOut = false;
		if (profileComplete) {
			double beyondEndTime = motionTime - profile.getEndTime();
			timedOut = beyondEndTime > MOTION_TIMEOUT_THRESHOLD;
		}

		// get profile set point
		profile.getSetpoint(time - motionStartTime, setPoint);

		// update current state
		currentState.time = Timer.getFPGATimestamp();
		currentState.position = getPosition();
		currentState.velocity = getVelocity();

		// update controller
		double motionCommand = controller.update(time, currentState, setPoint, (getShiftState() == LiftGear.HIGH) ? highGearCntrlParam : lowGearCntrlParam);
		boolean onTarget = controller.onTarget();


		// drive the motor
		double power = getHoldPower() + motionCommand / masterMotor.getBusVoltage();

		if (!stictionComplete) {
			if(Math.abs(currentState.velocity) < 10) {
				power += STATIC_FRICTION_POWER * Math.signum(setPoint.velocity);
			} else {
				stictionComplete = true;
			}
		}

		setPower(power);

		if (debug) {
			if (logIndex < logValues.length - 6) {
				logValues[logIndex++] = motionTime;
				logValues[logIndex++] = currentState.position;
				logValues[logIndex++] = currentState.velocity;
				logValues[logIndex++] = power;
				logValues[logIndex++] = setPoint.position;
				logValues[logIndex++] = setPoint.velocity;
			}
		}

		// if complete, switch to hold mode
		boolean motionComplete = (profileComplete && onTarget) || timedOut;

		if (motionComplete) {
			initializeHold(time);

			if (debug) {
				for(int i = 0; i < logIndex; ) {
					System.out.printf("%6.3f %6.3f %6.3f %6.3f %6.3f %6.3f\n",
							logValues[i++],
							logValues[i++],
							logValues[i++],
							logValues[i++],
							logValues[i++],
							logValues[i++]);
				}
			}
		}

		return motionComplete;
	}



	/**
	 * Return true if motion is complete
	 */
	public boolean isMotionComplete() {
		return liftState != LiftState.Motion;
	}

	private void setPower(double power){
		if(lockOut.isLiftLimited()) {
			power = Math.max(getShiftState() == LiftGear.LOW ? STALL_PERCENTAGE_LOW : STALL_PERCENTAGE_HIGH, power);
		}
		masterMotor.set(ControlMode.PercentOutput, power);
	}

	private double getCurrent() {
		return masterMotor.getOutputCurrent();
	}
	public double getVelocity() {
		return toInchesPerSecond(masterMotor.getSelectedSensorVelocity(0));
	}

	public double getPosition(){
		double position = toInches(masterMotor.getSelectedSensorPosition(0));
		if(position < 0.0) {
			setPosition(0);
			position = toInches(masterMotor.getSelectedSensorPosition(0));
		}
		return position;
	}
	/**
	 * 
	 * @param pulses
	 * @return inches
	 */
	private double toInches(double pulses) {
		return pulses * INCHES_PER_PULSE;
	}
	/**
	 * 
	 * @param Pulses Per 100 Ms
	 * @return Inches Per Second
	 */
	private double toInchesPerSecond(double pulsesPer100Ms) {
		return pulsesPer100Ms * INCHES_PER_PULSE * _100MS_PER_SECOND;
	}
	/**
	 * 
	 * @param velocity / inches per second
	 * @return ticks / 100ms
	 */

	//applies the gear that you want to the Shift
	public void setShift(LiftGear shift) {
		gearShifter.set(shift.value);
		gearState = shift;
	}

	//lets other classes know the current shift state
	public LiftGear getShiftState() {
		return gearState;
	}

	public void setPosition(int pos) {
		masterMotor.setSelectedSensorPosition(pos, 0, 0);
	}

	@Override 
	protected void initDefaultCommand() {
	}

	public static Lift getInstance() {
		return instance;
	}

	@Override
	public void init() {
		m_loop.startPeriodic(LIFT_CONTROL_PERIOD);

		SmartDashboard.putData("Test Lift Command", new TestLiftCommand());
		SmartDashboard.putBoolean("Lift DEBUG", false);
	}


	@Override
	public void update() {
		
		debug = SmartDashboard.getBoolean("Lift DEBUG", false);
		SmartDashboard.putNumber("Lift Current", getCurrent());
		SmartDashboard.putNumber("Height of lift", getPosition());
		SmartDashboard.putNumber("Lift Speed", getVelocity());
		SmartDashboard.putNumber("Lift Time Used", loopTimeUsed);
		SmartDashboard.putNumber("Lift Period",    loopPeriod);
	}

	public boolean isTimedOut() {
		return timedOut;
	}

	@Override
	public boolean isUnSafe() {
		return getPosition() < SAFE_HEIGHT;
	}
	
	public void endMotion() {
		initializeHold(Timer.getFPGATimestamp());
	}

}
