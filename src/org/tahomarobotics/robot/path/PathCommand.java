/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.path;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.auto.paths.PathConstants;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;
import org.tahomarobotics.robot.motion.MotionProfile.MotionProfileException;
import org.tahomarobotics.robot.motion.MotionProfile.MotionSection;
import org.tahomarobotics.robot.motion.MotionProfileController;
import org.tahomarobotics.robot.motion.MotionProfileController.ControllerParameters;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.state.FieldData;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotSpeed;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.State;

import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

public abstract class PathCommand extends Command {

	private static final Logger logger = Logger.getLogger(PathCommand.class.getSimpleName());

	private static final double CURVE_SEG_SIZE = Math.toRadians(5.0);

	public static enum RobotPosition {
		RIGHT, CENTER, LEFT;
	}

	private Waypoint lastPoint = null;
	
	private AdaptivePurePursuitController pathController;

	private final Chassis chassis;

	private final RobotState robotState;

	protected final FieldData fieldData;

	private State currentState = new State();

	private Path path = new Path();

	private MotionProfile motionProfiles[];

	private MotionState currentMotion = new MotionState();

	private MotionProfileController motionController;

	private Alliance alliance;

	private RobotPosition robotPosition;
	private final List<Command> onCompletionCommands = new ArrayList<>();
	private static final double kP = 1;
	private static final double kV = 0;
	private static final double kI = 0;
	private static final double kffV = 1.0;
	private static final double kffA = 0.0;
	private static final double POSITION_THRESHOLD = 2.0; 

	private final ControllerParameters motionControllerParameters = 
			new ControllerParameters(kP, kV, kI, kffV, kffA, POSITION_THRESHOLD);

	private static final double MAX_ACCEL = 100; // 10 ft/s^2

	private static final double MAX_JERK = 12 * 21;

	private static final double MOTION_TIMEOUT = 0.25;

	private List<MotionSection> sections = new ArrayList<>();

	private double pathDirection;
	
	protected final PathCommand beforeMe;

	public PathCommand(boolean test, Alliance alliance, RobotPosition robotPosition, double speed, PathCommand beforeMe) {
		this(0.0, test, alliance, robotPosition, speed, beforeMe, PathConstants.LOOKAHEAD_DISTANCE);
	}

	public PathCommand(boolean test, Alliance alliance, RobotPosition robotPosition, double speed) {
		this(0.0, test, alliance, robotPosition, speed, null, PathConstants.LOOKAHEAD_DISTANCE);
	}
	
	public PathCommand(double initialDirection, boolean test, Alliance alliance, RobotPosition robotPosition, double speed, PathCommand beforeMe) {
		this(initialDirection, test, alliance, robotPosition, speed, beforeMe, PathConstants.LOOKAHEAD_DISTANCE);	
	}

	public PathCommand(double initialDirection, boolean test, Alliance alliance, RobotPosition robotPosition, double speed, PathCommand beforeMe, double lookahead) {
		this.test = test;
		if(beforeMe == null) {
			this.pathDirection = initialDirection;
		}else {
			this.pathDirection = initialDirection;
			this.lastPoint = new Waypoint(beforeMe.getLastPose().x, beforeMe.getLastPose().y, 0.0);
			Waypoint reflectedWaypoint = reflectPoint(lastPoint, alliance, robotPosition);
			path.add(reflectedWaypoint);
					}
		this.robotPosition = robotPosition;
		this.alliance = alliance;
		this.beforeMe = beforeMe;
		if (!test) {
			chassis = Chassis.getInstance();
			requires(chassis);
			robotState = RobotState.getInstance();
			fieldData = FieldData.getInstance();
		} else {
			chassis = null;
			robotState = null;
			fieldData = null;
		}

		createPath();

		pathController = test ? null : new AdaptivePurePursuitController(path, lookahead, isReversed());	
		motionController = new MotionProfileController();
		try {
			motionProfiles = MotionProfile.createProfles(Profile.Trapazoid,  MAX_ACCEL, MAX_JERK, MotionProfile.combine(sections));
		} catch (MotionProfileException e) {
			e.printStackTrace();
		}
	}

	protected abstract void createPath();

	public List<Waypoint> getWaypoints() {
		List<Waypoint> waypoints = new ArrayList<>();
		for (Waypoint pt : path.getWaypoints()) {
			waypoints.add(new Waypoint(pt));
		}
		return waypoints;
	}

	protected void addWaypoint(Waypoint waypoint, CompletionListener listener) {
		Waypoint reflectedWaypoint = reflectPoint(waypoint, alliance, robotPosition);
		path.add(reflectedWaypoint);
		this.lastPoint = waypoint;
		if (listener != null) {
			reflectedWaypoint.addCompletionListener(listener);
		}
	}

	protected void addWaypoint(Waypoint waypoint) {
		addWaypoint(waypoint, null);
	}


	/**
	 * Add a line segment to the path at the given distance and with the speed constraint
	 * 
	 * @param length - distance in inches
	 * @param maxSpeed - maximum speed constraint in inches per second
	 */
	protected void addLine(double length, double maxSpeed, CompletionListener listener) {
		sections.add(new MotionSection(length, maxSpeed));
		Waypoint waypoint = new Waypoint(lastPoint);
		waypoint.x += length * Math.cos(pathDirection);
		waypoint.y += length * Math.sin(pathDirection);
		Waypoint reflectedWaypoint = reflectPoint(waypoint, alliance, robotPosition);
		path.add(reflectedWaypoint);
		this.lastPoint = waypoint;
		
		if (listener != null) {
			reflectedWaypoint.addCompletionListener(listener);
		}
	}

	protected void addLine(double length, double maxSpeed) {
		addLine(length, maxSpeed, null);
	}

	/**
	 * Add an arc to the path at the given angle and radius with a speed constraint
	 * 
	 * @param angle - angle in degrees with positive turning to the left
	 * @param radius - radius of the curve in inches
	 * @param maxSpeed - maximum speed constraint in inches per second
	 */
	protected void addArc(double angle, double radius, double maxSpeed, CompletionListener listener) {

		// segment-ize the curve to add waypoints to path
		Pose2D pt = new Pose2D(lastPoint.x, lastPoint.y, pathDirection);
		int numSegments = (int)(Math.abs(angle)/CURVE_SEG_SIZE) + 1;
		double deltaAngle = angle / numSegments;
		Waypoint lastReflected = null;
		for (int i = 1; i <= numSegments; i++) {
			pt = getPoint(pt, deltaAngle, 1.0/radius);
			Waypoint waypoint = new Waypoint(pt.x, pt.y, 0);
			Waypoint reflectedWaypoint = reflectPoint(waypoint, alliance, robotPosition);
			path.add(reflectedWaypoint);
			lastReflected = reflectedWaypoint;
			lastPoint = waypoint;
			pathDirection = pt.heading;

		}
		sections.add(new MotionSection(Math.abs(angle) * Math.abs(radius), maxSpeed));
		if (listener != null) {
			lastReflected.addCompletionListener(listener);
		}
	}
	
	protected void addArc(double angle, double radius, double maxSpeed) {
		addArc(angle, radius, maxSpeed, null);
	}

	/**
	 * Create arc with end point
	 * 
	 * @param angle - turn angle (radians), positive is anti-clockwise
	 * @param x - ending x position
	 * @param y - ending y position
	 * @param maxSpeed
	 */
	protected void addArcToPoint(double x, double y, double maxSpeed, CompletionListener listener) {
		
		// reflect point and angle
		Waypoint point = new Waypoint(x, y, maxSpeed);
		
		// calculate radius
		double dist = point.distance(lastPoint);
		double angle = normalizeAngle(point.angle(lastPoint) - normalizeAngle(pathDirection));
		
		double radius = Math.abs((dist / 2) / Math.sin(angle));
		
		// create arc
		addArc(2 * angle, radius, maxSpeed, listener);
	}
	
	private double normalizeAngle(double angle)
	{
		double newAngle = angle;
	    while (newAngle <= -Math.PI) newAngle += Math.PI * 2;
	    while (newAngle > Math.PI) newAngle -= Math.PI * 2;
	    return newAngle;
	}
	/**
	 * Create arc with end point and completion listener
	 * 
	 * @param angle - turn angle (radians), positive is anti-clockwise
	 * @param x - ending x position
	 * @param y - ending y position
	 * @param maxSpeed
	 * @param listener
	 */
	protected void addArcToPoint(double x, double y, double maxSpeed) {
		addArcToPoint(x, y, maxSpeed, null);
	}

	private Pose2D getPoint(Pose2D from, double angle, double curvature) {

		double endPointLen = Math.abs(angle)/curvature;

		double endPointAngle = from.heading + angle/2;

		return new Pose2D(
				from.x + endPointLen * Math.cos(endPointAngle), 
				from.y + endPointLen * Math.sin(endPointAngle),
				from.heading + angle);
	}

	public abstract boolean isReversed();

	private final RobotSpeed speedCommand = new RobotSpeed();

	private double pathStart;

	private final MotionState setpoint = new MotionState();

	private int profileIndex;

	private boolean motionComplete;

	private final boolean test;


	@Override
	protected void initialize() {
		pathStart = getTime();
		profileIndex = 0;
		motionComplete = false;
		motionController.reset();
	}

	private double time = 0.0;

	private double getTime() {
		if (test) {
			time += 0.01;
			return time;
		}
		time = Timer.getFPGATimestamp();
		return time;
	}
	/**
	 * With the current robot pose, determine the steering and motion commands.  Combine the commands
	 * to control the robot with velocity commands.
	 */
	@Override
	protected void execute() {

		double elapsedTime = (test ? getTime() : Timer.getFPGATimestamp()) - pathStart;

		// get the latest pose from RobotState
		if (robotState != null) {
			robotState.getState(currentState);
		}

		// steering command is the required curvature (1/radius of a circle)
		double steeringCommand = pathController == null ? 0 : pathController.update(currentState.pose);

		if(!motionComplete) {
			boolean success = motionProfiles[profileIndex].getSetpoint(elapsedTime, setpoint);
			if (!success) {
				profileIndex++;
				if (profileIndex < motionProfiles.length) {
					success = motionProfiles[profileIndex].getSetpoint(elapsedTime, setpoint);
				} else {
					motionComplete = true;
					setTimeout(timeSinceInitialized() + MOTION_TIMEOUT);
					logger.log(Level.INFO, "Motion complete");
				}
			}
		} 

		if (test) {
			System.out.println(setpoint);
			return;
		}

		// get set point from motion profiles
		currentMotion.time = elapsedTime;
		currentMotion.position = pathController == null ? 0 : pathController.getDistance();
		currentMotion.velocity = currentState.velocity.forward;
		currentMotion.acceleration = currentState.getAcceleration();

		chassis.setWantedVelocity(setpoint.velocity * (isReversed() ? -1 : 1));
		//motion command is the velocity from the motion profile controller
		double motionCommand = motionController.update(elapsedTime, currentMotion, setpoint, motionControllerParameters);

		// command the motion
		speedCommand.forward = (isReversed() ? -1 : 1) * motionCommand;
		speedCommand.rotational = motionCommand * steeringCommand;
		chassis.setPositionError(setpoint.position - currentMotion.position);
		chassis.setSpeed(speedCommand);
	}

	@Override
	protected boolean isFinished() {
		boolean isCompleted = motionComplete && (motionController.onTarget() || test || isTimedOut());
	
		if (isCompleted) {
			for (Command cmd : onCompletionCommands) {
				if (!cmd.isCompleted()) {
					return false;
				}
			}
		}
		return isCompleted;
		
	}

	@Override
	protected void end() {
		if (chassis != null) {
			chassis.setPower(0, 0);
		}
		logger.log(Level.INFO, String.format("Path Command Completed %6.3f", timeSinceInitialized()));

		if(isTimedOut()) {
			logger.log(Level.INFO, "Path Command Timed out");
		}
	}

	protected static class Point {
		double x;
		double y;
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}

	public String serialize() {
		// The output should look something like "Path{Waypoint{0,1},Waypoint{8,15.36520}}"
		String result = "Path{";
		for (Waypoint w : getWaypoints()) {
			result += "Waypoint{" + w.x + "," + w.y + "}";
		}
		return result + "}";
	}


	public static Waypoint reflectPoint(Waypoint pt, Alliance alliance, RobotPosition position) {

		if (alliance == Alliance.Blue && position == RobotPosition.LEFT) {
			return new Waypoint(pt.x, PathConstants.FIELD_WIDTH - pt.y, pt.speed);
		}
		if (alliance == Alliance.Red && position == RobotPosition.RIGHT) {
			return new Waypoint(PathConstants.FIELD_LENGTH - pt.x, PathConstants.FIELD_WIDTH - pt.y, pt.speed);
		}
		if (alliance == Alliance.Red && position == RobotPosition.LEFT) {
			return new Waypoint(PathConstants.FIELD_LENGTH - pt.x, pt.y, pt.speed);
		}
		if(alliance == Alliance.Red && position == RobotPosition.CENTER) {
			return new Waypoint(PathConstants.FIELD_LENGTH - pt.x, PathConstants.FIELD_WIDTH - pt.y, pt.speed);
		}

		return pt;
	}

	public static double reflectHeading(double heading, Alliance alliance, RobotPosition position) {
		heading += alliance == Alliance.Blue ? 0 : Math.PI;
		return reflectTurnAngle(heading, position);

	}

	public static double reflectTurnAngle(double turnAngle, RobotPosition position) {
		return position == RobotPosition.LEFT ? -turnAngle : turnAngle;
	}

	public void test() {
		initialize();
		while(!isFinished()) {
			execute();
		}
		end();
	}

	public static Pose2D relfectPose(Pose2D pose, RobotPosition position, Alliance alliance) {

		Waypoint newPoint = reflectPoint(new Waypoint(pose.x, pose.y, 0.0), alliance, position);
		double heading = reflectHeading(pose.heading, alliance, position);
		pose = new Pose2D(newPoint.x, newPoint.y, heading);

		return pose;
	}

	public Pose2D getLastPose() {
		return new Pose2D(lastPoint.x, lastPoint.y, pathDirection);
	}
	
	
	public void addCompletionCommand(Command onCompletionCommand) {
		onCompletionCommands.add(onCompletionCommand);
	}
}
