/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.arm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.RobotConfiguration.RobotConfigurationIF;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.arm.test.ArmModel;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileController;
import org.tahomarobotics.robot.motion.MotionProfileController.ControllerParameters;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.LockOut;
import org.tahomarobotics.robot.util.LockOut.LockOutIF;
import org.tahomarobotics.robot.util.UpdaterIF;

import com.ctre.phoenix.motorcontrol.ControlFrame;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Arm extends Subsystem implements UpdaterIF, LockOutIF {

	public enum ArmState {
		Hold,
		Motion,
		Motion_Start,
		Manual,
		Power,
		HoldStart,
		DPadMovement;
	}

	private static final Logger logger = Logger.getLogger(Arm.class.getSimpleName());

	private boolean isLockedOut = false;

	private static final RobotConfigurationIF robotConfiguration = RobotConfiguration.getInstance();
	private final LockOut lockOut = LockOut.getInstance();

	private final TalonSRX armMotor = new TalonSRX(RobotMap.ARM_MOTOR);

	private static final double DEGREES_PER_PULSE = 4096 / 360;
	private static final double ZERO_ENCODER_VALUE = robotConfiguration.getArmAngleOffset();

	private static final double MOTION_kP = 0;
	private static final double MOTION_kV  = 0;
	private static final double MOTION_kI  = robotConfiguration.getArmKI();

	private static final double POSITION_TOLERANCE = Math.toRadians(1.0);

	private static final double HORIZONTAL_STALL_POWER_NO_CUBE = RobotConfiguration.getInstance().getArmStall();
	private static final double HORIZONTAL_STALL_POWER_CUBE = RobotConfiguration.getInstance().getArmCubeStall();

	private final ControllerParameters controllerParameters = new ControllerParameters(
			MOTION_kP, MOTION_kV, MOTION_kI, ArmModel.kFFV * robotConfiguration.getArmVelocityFudgeFactor(),
			ArmModel.kFFA * robotConfiguration.getArmAccelerationFudgeFactor(), POSITION_TOLERANCE);

	private final MotionProfileController controller = new MotionProfileController();

	private static final Arm instance = new Arm();
	private static final double ARM_CONTROL_PERIOD = 0.005;
	private static final double _100MS_PER_SECOND = 10;
	private static final double MICROSEC_PER_SECOND = 1.0e6;
	private static final double MOTION_TIMEOUT_THRESHOLD = 0.25;
	private static final double ARM_INPUT_DERATE = 0.5;
	private static final double ARM_INPUT_DEADBAND = 0.2;

	private static final double MAX_ANGLE = Math.toRadians(80);
	private static final double BOTTOM_ANGLE = Math.toRadians(-40);

	private static final double STATIC_FRICTION_POWER = robotConfiguration.getArmStaticFriction();
	private static final double STICTION_VELOCITY_LIMIT = Math.toRadians(10);

	private static final double _2PI = 2.0 * Math.PI;

	private volatile double powerArm;

	private volatile ArmState armState = ArmState.Hold;
	private volatile int loopTimeUsed = 0;
	private volatile int loopPeriod = 0;

	//Motion states for use with the controller
	private final MotionState currentState = new MotionState();
	private final MotionState setPoint = new MotionState();

	private double prevTime = Timer.getFPGATimestamp();

	private final AtomicReference<MotionProfile> profileReference = new AtomicReference<>();

	private MotionProfile profile = null;
	private volatile boolean timedOut = false;

	private volatile boolean debug = false;

	private boolean forceCubeDetect = false;

	private Arm() {
		lockOut.setArm(this);

		armMotor.setNeutralMode(NeutralMode.Brake);

		armMotor.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_5Ms, 0);
		armMotor.configVelocityMeasurementWindow(1, 0);
		armMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 1, 0);
		armMotor.setControlFramePeriod(ControlFrame.Control_3_General, 5);
		armMotor.configNeutralDeadband(0.01, 0);
		armMotor.setInverted(true);
		armMotor.setSensorPhase(true);

		armMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);

		setCurrentLimit(false);
	}

	public void forceCubeDetect() {
		forceCubeDetect = true;
	}

	public void clearForceCubeDetect() {
		forceCubeDetect = false;
	}

	public void setCurrentLimit(boolean isClimbing) {

		int peak = isClimbing ? 30 : 20;
		int cont = isClimbing ? 20 : 10;

		/**
		 * Once peak current has been exceeded for the duration, the current is limited down
		 * to the continuous current limit.  Current is specified in Amps and duration
		 * is specified in milliseconds.
		 */
		armMotor.configPeakCurrentLimit(peak, 0);
		armMotor.configPeakCurrentDuration(2000, 0); 
		armMotor.configContinuousCurrentLimit(cont, 0); 
		armMotor.enableCurrentLimit(true);

	}


	private Notifier m_loop = new Notifier(new Runnable() {
		@Override
		public void run() {
			runControlLoop();
		}
	});


	/**
	 * Main Arm control loop
	 */
	private void runControlLoop() {

		double time = Timer.getFPGATimestamp();

		switch(armState) {

		case Hold:
			holdControl(time);
			if(manualControl()) {
				armState = ArmState.Manual;
			}
			
			if(dpadControl(time)) {
				armState = ArmState.DPadMovement;
			}
			
			break;

		case Motion_Start:
			logger.log(Level.INFO, "Starting Motion Profile");
			profile = profileReference.get();
			armState = ArmState.Motion;
			synchronized(logData) {
				logData.clear();
			}

			controller.reset();
			motionStartTime = time;
			stictionComplete = false;
			// Fall-through to Motion

		case Motion:
			motionControl(time);
			break;

		case Manual:
			if(!manualControl()) {
				logger.log(Level.INFO, String.format("Manual Complete -> %4.1f", Math.toRadians(getAngle())));
				initializeHold(time);
			}
			break;

		case DPadMovement:
			if(!dpadControl(time)) {
				logger.log(Level.INFO, String.format("DPAD Movement Complete -> %4.1f", Math.toRadians(getAngle())));
				initializeHold(time);
			}
			break;
			
		case Power:
			setPower(powerArm);
			break;

		case HoldStart:
			initializeHold(time);
			break;
		}

		double end = Timer.getFPGATimestamp();
		loopTimeUsed = (int) ((end - time) * MICROSEC_PER_SECOND);
		loopPeriod = (int) ((time - prevTime) * MICROSEC_PER_SECOND);
		prevTime = time;
	}

	public void setPowerState(double powerArm) {

		this.powerArm = powerArm;
		armState = (powerArm == 0.0) ? ArmState.Hold : ArmState.Power;

	}

	private void initializeHold(double time) {
		setPoint.position = getAngle();
		setPoint.velocity = 0;
		setPoint.acceleration = 0;
		setPoint.jerk = 0;
		armState = ArmState.Hold;
		holdControl(time);
	}

	/**
	 * Checks to see if there is any input from the controller
	 * @return true if input is applied
	 */
	private boolean manualControl() {
		double armInput = getArmInput();
		boolean hasInput = Math.abs(armInput) > 0;
		if(hasInput) {
			double angle = getAngle();
			if(angle < BOTTOM_ANGLE && armInput < 0) {
				hasInput = false;
			}
		}

		if(hasInput) {
			setPower(getHoldPower() + armInput * ARM_INPUT_DERATE);
		}

		return hasInput;
	}
	
	private boolean dpadControl(double time) {
		switch(OI.getInstance().getDriverPOV()) {
		case 90:
			setPower(getHoldPower() + 0.2);
			break;
		case 270:
			setPower(getHoldPower() + -0.2);
			break;
		default:
			return false;
		}
		
		return true;
	}

	/**
	 * Get the input from OI and applies deadband
	 * @return the input with deadband applied
	 */
	private double getArmInput() {
		return OI.applyDeadBand(OI.getInstance().getManipulatorLeftYAxis(), ARM_INPUT_DEADBAND);
	}

	private volatile double motionStartTime = 0;

	public boolean startMotion(MotionProfile profile) {

		this.profileReference.set(profile);
		armState = ArmState.Motion_Start;
		return true;
	}

	private double getHoldPower() {
		double stallMax = (/*Collector.getInstance().isCollected() || */forceCubeDetect) ? HORIZONTAL_STALL_POWER_CUBE : HORIZONTAL_STALL_POWER_NO_CUBE;
		return Math.cos(getAngle()) *  stallMax;
	}

	private void holdControl(double time) {
		setPower(getHoldPower());
	}

	private boolean stictionComplete;

	private final List<LogData> logData = Collections.synchronizedList(new ArrayList<>());

	private boolean motionControl(double time) {
		//subtract motion start time
		double motionTime = time - motionStartTime;
		boolean profileComplete = motionTime > profile.getEndTime();

		timedOut = false;
		if(profileComplete) {
			double beyondEndTime = motionTime - profile.getEndTime();
			timedOut = beyondEndTime > MOTION_TIMEOUT_THRESHOLD;
		}

		//get profile set point
		profile.getSetpoint(time - motionStartTime, setPoint);

		//update current state
		currentState.time = time;
		currentState.position = getAngle();
		currentState.velocity = getVelocity();

		//update controller
		double motionCommand = controller.update(time, currentState, setPoint, controllerParameters);
		boolean onTarget = controller.onTarget();

		//drive the motor
		double power = getHoldPower() + motionCommand / armMotor.getBusVoltage();

		if (!stictionComplete) {
			if(Math.abs(currentState.velocity) < STICTION_VELOCITY_LIMIT) {
				power += STATIC_FRICTION_POWER * Math.signum(setPoint.velocity);
			} else {
				stictionComplete = true;
			}
		}

		setPower(power);
		if (debug) {
			synchronized(logData) {
				logData.add(new LogData(motionTime, currentState.position, currentState.velocity, power, setPoint.position, setPoint.velocity));
			}
		}

		//if complete, switch to hold mode
		boolean motionComplete = (profileComplete && onTarget) || timedOut;

		if(motionComplete) {
			initializeHold(time);
			//printLogData();
		}

		return motionComplete;

	}

	/** 
	 * @return true if motion is complete
	 */
	public boolean isMotionComplete() {
		return !(armState == ArmState.Motion || armState == ArmState.Motion_Start);
	}

	public static Arm getInstance() {
		return instance;
	}

	private double normalizeAngle(double radians) {
		return (radians + Math.PI) % _2PI - Math.PI;
	}

	public double getAngle() {
		return normalizeAngle(Math.toRadians(ticksToDegrees(armMotor.getSelectedSensorPosition(0)) - ZERO_ENCODER_VALUE));
	}

	public double getVelocity() {
		return Math.toRadians(ticksToDegrees(armMotor.getSelectedSensorVelocity(0)) * _100MS_PER_SECOND);
	}

	private double ticksToDegrees(double input) {
		return input / DEGREES_PER_PULSE;
	}

	public double getCurrent() {
		return armMotor.getOutputCurrent();
	}


	@Override
	public void init() {
		m_loop.startPeriodic(ARM_CONTROL_PERIOD);

		SmartDashboard.putData("Test Arm Command", new TestArmCommand());
		SmartDashboard.putBoolean("Arm DEBUG", false);
	}

	@Override
	public void update() {
		debug = SmartDashboard.getBoolean("Arm DEBUG", false);
		SmartDashboard.putNumber("Arm Current", armMotor.getOutputCurrent());
		SmartDashboard.putNumber("Arm Position", Math.toDegrees(getAngle()));
		SmartDashboard.putNumber("Arm Time Used", loopTimeUsed);
		SmartDashboard.putNumber("Arm Period",    loopPeriod);
	}

	@Override
	protected void initDefaultCommand() {
	}

	protected class LogData {
		public final double time;
		public final double position;
		public final double velocity;
		public final double power;
		public final double expectedPosition;
		public final double expectedVelocity;

		public LogData(double time, double position, double velocity, double power, double expectedPosition, double expectedVelocity) {
			this.time = time;
			this.position = position;
			this.velocity = velocity;
			this.power = power;
			this.expectedPosition = expectedPosition;
			this.expectedVelocity = expectedVelocity;
		}

		@Override
		public String toString() {
			return String.format("%6.3f %6.3f %6.3f %6.3f %6.3f %6.3f", 
					time, Math.toDegrees(position), Math.toDegrees(velocity),
					power, Math.toDegrees(expectedPosition), Math.toDegrees(expectedVelocity));
		}


	}

	public boolean isTimedOut() {
		return timedOut;
	}

	public void printLogData() {
		if (debug) {
			synchronized(logData) {
				for(LogData data : logData) {
					System.out.println(data);		
				}
			}
		}
	}

	private void setPower(double power) {
		if(lockOut.isArmLimited() && getAngle() > MAX_ANGLE) {
			power = Math.min(getHoldPower(), power);
			
			if(!isLockedOut) {
				logger.log(Level.INFO, "Arm is locked out");
				isLockedOut = true;
			}
		} else if(isLockedOut) {
			logger.log(Level.INFO, "Arm is Unlocked");
			isLockedOut = false;
		}
		armMotor.set(ControlMode.PercentOutput, power);
	}

	@Override
	public boolean isUnSafe() {
		return getAngle() > MAX_ANGLE;
	}

}