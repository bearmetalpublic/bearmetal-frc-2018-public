/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.arm.test;

import java.util.HashMap;
import java.util.Map;

import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;
import org.tahomarobotics.robot.motion.MotionProfileController;
import org.tahomarobotics.robot.motion.MotionProfileController.ControllerParameters;
import org.tahomarobotics.robot.motion.MotionState;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestArmModel extends Application {

	@SuppressWarnings("unused")
	private static final double TRAVEL = Math.toRadians(90);
	private static final double MAX_VELOCITY = Math.toRadians(360);
	private static final double MAX_ACCEL = Math.toRadians(500);
	private static final double MAX_JERK = 30000;
	
	private final ArmModel model = new ArmModel();
	
	private final Map<String, XYChart.Series<Number,Number>> series = new HashMap<>();
	
	private void addData(String name, double time, double value) {
		XYChart.Series<Number,Number> data = series.get(name);
		if (data == null) {
			data = new XYChart.Series<>();
			data.setName(name);
			series.put(name, data);
		}
		data.getData().add(new XYChart.Data<Number,Number>(time, value));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Conditions k = new Conditions(Profile.BetterSCurve, Math.toRadians(0), Math.toRadians(60), 0, 0, MAX_VELOCITY, MAX_ACCEL, MAX_JERK);
		MotionProfile profile = new MotionProfile(0, k);

		double endTime = profile.getEndTime();
		
		double yMax = 0;
		double yMin = 0;
		final MotionState setpoint = new MotionState();
		final MotionState currentState = new MotionState();
		final ControllerParameters param = new ControllerParameters(0, 0, 0, ArmModel.kFFV, ArmModel.kFFA, 1.0) ;
		final MotionProfileController controller = new MotionProfileController();
		//System.out.format("time voltage current torque acceleration, velocity, position, motorVelocity, emf\n");
		double command = 0;
		int count = 0;
		for (double time = 0; time <= endTime; time += 0.001) {
			boolean success = profile.getSetpoint(time, setpoint);
			if (success) {
				currentState.position = model.position;
				yMax = Math.max(yMax, Math.toDegrees(setpoint.position));
				yMax = Math.max(yMax, Math.toDegrees(currentState.position));
				yMin = Math.min(yMin, Math.toDegrees(setpoint.position));
				yMin = Math.min(yMin, Math.toDegrees(currentState.position));
				currentState.velocity = model.velocity;
				yMax = Math.max(yMax, Math.toDegrees(setpoint.velocity));
				yMax = Math.max(yMax, Math.toDegrees(currentState.velocity));
				yMin = Math.min(yMin, Math.toDegrees(setpoint.velocity));
				yMin = Math.min(yMin, Math.toDegrees(currentState.velocity));
				currentState.acceleration = model.acceleration;
				
				if (count % 5 == 0) {
					command = controller.update(time, currentState, setpoint, param) + ArmModel.kFFP * Math.cos(currentState.position);
					command = Math.signum(command) * Math.min(Math.abs(command), ArmModel.MAX_VOLTAGE);
				}
				count++;
				
				addData("Position (cmd)", time, Math.toDegrees(setpoint.position));
				addData("Velocity (cmd)", time, Math.toDegrees(setpoint.velocity));
				addData("Voltage", time, model.voltage);
				addData("Position (actual)", time, Math.toDegrees(currentState.position));
				addData("Velocity (actual)", time, Math.toDegrees(currentState.velocity));
				
				System.out.format("%6.3fs %6.3fin %6.3fin/s %6.3fin/s2 %6.3fV %6.3fA \n", 
						time, currentState.position, currentState.velocity, currentState.acceleration,
						model.voltage, model.current);
				
				model.update(time, command);
			}
		}
		
		
		double xMin = 0;
		double xMax = endTime;
		yMax *= 1.1;
		yMin *= 1.1;
		
		primaryStage.setTitle("Lift Motion");
        final NumberAxis xAxis = new NumberAxis(xMin, xMax, 0.1);
        final NumberAxis yAxis = new NumberAxis(yMin, yMax, 5);        
        final LineChart<Number,Number> sc = new LineChart<>(xAxis,yAxis);
        xAxis.setLabel("Time (sec)");                
        yAxis.setLabel("Velocity (inch/sec)");
        sc.setTitle("Lift Motion");

        sc.setPrefSize(1000, 700);
        sc.getData().addAll(series.values());
        Scene scene  = new Scene(new Group());
        final VBox vbox = new VBox();
        final HBox hbox = new HBox();
               
        hbox.setSpacing(10);
        
        vbox.getChildren().addAll(sc);
        hbox.setPadding(new Insets(10, 10, 10, 50));
        
        ((Group)scene.getRoot()).getChildren().add(vbox);
        
        scene.getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        
        primaryStage.setScene(scene);
        primaryStage.show();
		
	}
 
	public static void main(String[] args) {
        launch(args);
    }

}
