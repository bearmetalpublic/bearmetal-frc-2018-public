/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.arm.test;

public class ArmModel {
	
	private static final double SPEC_VOLTAGE = 12.0;
	private static final double FREE_SPEED = 18730.0; // RPM
	private static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec 
	private static final double FREE_CURRENT = 0.7; // Amps
	private static final double STALL_TORQUE = 0.71; // N-m
	private static final double STALL_CURRENT = 134; // Amps
	private static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
	private static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT);
	private static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT);
	
	private static final double GEAR_STAGE1 = 100.0/12.0;
	private static final double GEAR_STAGE2 = 60.0/20.0;
	private static final double GEAR_STAGE3 = 60.0/20.0;
	private static final double GEAR_STAGE4 = 56.0/16.0;
	
	private static final double GEAR_RATIO = GEAR_STAGE1 * GEAR_STAGE2 * GEAR_STAGE3 * GEAR_STAGE4;

	private static final int MOTOR_COUNT = 1;
	private static final double MASS = 10.0 * 0.453592; // lbs to kgs
	private static final double CUBE_MASS = 3.0 * 0.453592; // lbs to kgs
	private static final double CG = 9 * 0.0254;
	
	private static final double EFFICIENCY = 0.80;
	private static final double GRAVITY = 9.80665; // m/s2 
	
	public static final double MAX_VOLTAGE = 12.5;
	
	public static final double kFFP = GRAVITY * MASS * CG * RESISTANCE / kT / MOTOR_COUNT / GEAR_RATIO / EFFICIENCY;
	public static final double kFFV = GEAR_RATIO / kV;
	public static final double kFFA = MASS * CG * CG * RESISTANCE / kT / MOTOR_COUNT / GEAR_RATIO / EFFICIENCY;

	public static final double I_EMPTY = MASS * CG * CG;
	public static final double I_WITH_CUBE = (CUBE_MASS + MASS) * CG * CG;
	public static final double I = I_EMPTY;
	public double time;
	public double voltage;
	public double current;
	public double torque;
	public double acceleration; // angular acceleration (rad/s2)
	public double velocity; // angular speed (rad/s)
	public double position; // angle (rad)
	public double motorVelocity;
	public double emf;
	
	public void update(double time, double voltage) {
		
		double dT = time - this.time;
		this.time = time;
		
		this.voltage = voltage;
		current = (voltage - emf) / RESISTANCE;
		torque = current * kT;
		double acceleration = (torque * MOTOR_COUNT * GEAR_RATIO * EFFICIENCY - MASS * GRAVITY * CG * Math.cos(position))  / I ;
		double velocity = this.velocity + (acceleration + this.acceleration)/2 * dT;
		this.acceleration = acceleration;
		position += (velocity + this.velocity)/2 * dT;
		this.velocity = velocity;
		motorVelocity = velocity * GEAR_RATIO;
		emf = motorVelocity / kV;
		
//		System.out.format("%6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f\n", 
//				time, voltage, current, torque, 
//				acceleration, velocity, position, motorVelocity, emf);
	}

}
