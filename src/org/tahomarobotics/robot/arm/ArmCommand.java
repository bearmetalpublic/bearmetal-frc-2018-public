/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.arm;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;
import org.tahomarobotics.robot.motion.MotionProfile.MotionProfileException;

import edu.wpi.first.wpilibj.command.Command;

public class ArmCommand extends Command {

	public static final double LOWEST = -60;
	public static final double COLLECT = -35.0;
	public static final double STOW = 32.0;
	public static final double SCORE = 32.0;
	public static final double PROTECTED = 68.0;
	public static final double CLIMB = 90.0;
	public static final double REVERSE = 120.0;
	public static final double HIGHEST = 180;
	public static final double SAFE_LIFT_MOVEMENT = STOW + 10.0;

	private static final Logger logger = Logger.getLogger(ArmCommand.class.getSimpleName());

	private static double MAX_VELOCITY = Math.toRadians(360);
	private static final double MAX_ACCELERATION = Math.toRadians(500);
	private static final double MAX_JERK = Math.toRadians(5000);

	private Arm arm = Arm.getInstance();

	private MotionProfile profile;

	private final double position;
	private final double greaterThan;
	private final double lessThan;
	private boolean ifOutOfRange;
	private boolean areInitConditionsMeet = false;
	
	/**
	 * 
	 * @param position in degrees
	 */
	public ArmCommand(double position) {
		this.position = Math.toRadians(position);
		greaterThan = Math.toRadians(LOWEST);
		lessThan = Math.toRadians(HIGHEST);
		
	}

	/**
	 * 
	 * @param position in degrees
	 * @param greaterThan in degrees
	 * @param lessThan in degrees
	 */
	public ArmCommand(double position, double greaterThan, double lessThan) {
		this.position = Math.toRadians(position);
		this.greaterThan = Math.toRadians(greaterThan);
		this.lessThan = Math.toRadians(lessThan);
	}
	
	/**
	 * 
	 * @param position in degrees
	 * @param greaterThan in degrees
	 * @param lessThan in degrees
	 * @param orConditions Sets the command to check if greaterThan OR lessThan, rather than AND
	 */
	public ArmCommand(double position, double greaterThan, double lessThan, boolean orConditions) {
		this(position, greaterThan, lessThan);
		this.ifOutOfRange = orConditions;
	}
	
	public ArmCommand(double position, double greaterThan, double lessThan, boolean orConditions, double maxVelocity) {
		this(position, greaterThan, lessThan);
		this.ifOutOfRange = orConditions;
		MAX_VELOCITY = maxVelocity;
	}
	
	protected void initialize(double position) {
		double currentPosition = arm.getAngle();
		
		if(!ifOutOfRange) {	
			areInitConditionsMeet = currentPosition > greaterThan && currentPosition < lessThan;
		} else {
			areInitConditionsMeet = currentPosition > greaterThan || currentPosition < lessThan;
		}
		
		if(areInitConditionsMeet) {
			Conditions constraint = new Conditions(
					Profile.BetterSCurve, arm.getAngle(),
					position, 0,
					0, MAX_VELOCITY,
					MAX_ACCELERATION, MAX_JERK);
			try {
			
				profile = new MotionProfile(0, constraint);
				arm.startMotion(profile);
			
			} catch (MotionProfileException e) {
				logger.log(Level.SEVERE, String.format("Arm Profile failed with %6.3f %6.3f", Math.toDegrees(currentPosition), Math.toDegrees(position)), e);
				areInitConditionsMeet = false;
			}

			
		}
	}

	@Override
	protected void initialize() {
		initialize(position);
	}

	@Override
	protected boolean isFinished() {
		if(!areInitConditionsMeet) {
			logger.log(Level.INFO, "Arm Command init conditions met. Arm angle: " + Math.toDegrees(arm.getAngle()));
			return true;
		}
		boolean finished = arm.isMotionComplete();

		if (finished) {
			logger.log(Level.INFO, "Arm Command expected " + Math.toDegrees(profile.getEndPosition()) + " actual " + Math.toDegrees(arm.getAngle()) + (arm.isTimedOut() ? " TimedOut" : ""));
			arm.printLogData();

		}
		return finished;
	}

	@Override
	protected void end() {
		logger.log(Level.INFO, String.format("Arm Command Completed %6.3f", timeSinceInitialized()));

	}
}