/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class RobotConfiguration {

	private static RobotConfigurationIF instance;

	public static interface RobotConfigurationIF {
		double getArmAngleOffset();
		double getArmStall();
		double getArmCubeStall();
		double getLiftHighStall();
		double getLiftLowStall();
		double getLeftKFFV();
		double getRightKFFV();
		double getArmStaticFriction();
		double getArmKP();
		double getArmKI();
		double getArmAccelerationFudgeFactor();
		double getLiftVelocityFudgeFactor();
		double getLiftStaticFriction();
		double getLiftKP();
		double getLiftKI();
		double getLiftKV();
		double getLiftAccelerationFudgeFactor();
		double getArmVelocityFudgeFactor();
		double getWheelDia();
	}

	@SuppressWarnings("unused")
	private static final String practiceMacAddress = "null0-1284723-25106";
	@SuppressWarnings("unused")
	private static final String competitionMacAddress = "null0-12847241985";

	static {
		/*String macAddress = getRobotMacAddress();

		if (macAddress.equals(practiceMacAddress)) {
			System.out.println("Practice Robot Detected");
			instance = new RobotConfigurationIF() {

				@Override
				public double getArmAngleOffset() {
					return 263.5;
				}
				@Override
				public double getArmStall() {
					return 0.10;
				}
				@Override
				public double getLiftHighStall() {
					return 0.14;
				}
				@Override
				public double getLiftLowStall() {
					return 0.0625;	
				}
				@Override
				public double getLeftKFFV() {
					return .7995 * 1023 / 2785 * 1.2;
				}
				@Override
				public double getRightKFFV() {
					return .7995 * 1023 / 2811 * 1.2;
				}
				@Override
				public double getArmCubeStall() {
					return 0.17;
				}
				@Override
				public double getArmStaticFriction() {
					return 0.12;
				}
				@Override
				public double getArmKI() {
					return 0.0;
				}
				@Override
				public double getArmKP() {
					return 0.0;
				}
				@Override
				public double getArmAccelerationFudgeFactor() {
					return 4;
				}
				@Override
				public double getArmVelocityFudgeFactor() {
					return 1.05;
				}
				@Override
				public double getLiftStaticFriction() {
					return 0.5;
				}
				@Override
				public double getLiftKP() {
					return 0.5;
				}
				@Override
				public double getLiftKI() {
					return 1.0;
				}
				@Override
				public double getLiftKV() {
					return 0.0;
				}
				@Override
				public double getLiftAccelerationFudgeFactor() {
					return 1.3;
				}
				@Override
				public double getLiftVelocityFudgeFactor() {
					return 0.60;
				}
				@Override
				public double getWheelDia() {
					return 6.029179 * 0.93;
				}
			};
		} else if (macAddress.equals(competitionMacAddress)) {
			System.out.println("Competition Robot Detected");
*/
			instance = new RobotConfigurationIF() {

				@Override
				public double getArmAngleOffset() {
					return -54.5;
				}
				@Override
				public double getArmStall() {
					return 0.08;
				}
				@Override
				public double getLiftHighStall() {
					return 0.17;
				}
				@Override
				public double getLiftLowStall() {
					return 0.1;
				}
				@Override
				public double getLeftKFFV() {
					return .7995 * 1023 / 2785;
				}
				@Override
				public double getRightKFFV() {
					return .7995 * 1023 / 2811;
				}
				@Override
				public double getArmCubeStall() {
					return 0.15;
				}
				@Override
				public double getArmStaticFriction() {
					return 0.12;
				}
				@Override
				public double getArmKI() {
					return 4.0;
				}
				@Override
				public double getArmKP() {
					return 0.1;
				}
				@Override
				public double getArmAccelerationFudgeFactor() {
					return 4.0;
				}
				@Override
				public double getArmVelocityFudgeFactor() {
					return 0.7;
				}
				@Override
				public double getLiftStaticFriction() {
					return 0.05;
				}
				@Override
				public double getLiftKP() {
					return 1.0;
				}
				@Override
				public double getLiftKI() {
					return 2.0;
				}
				@Override
				public double getLiftKV() {
					return 0.0;
				}
				@Override
				public double getLiftAccelerationFudgeFactor() {
					return 3.2;
				}
				@Override
				public double getLiftVelocityFudgeFactor() {
					return 0.75;
				}
				@Override
				public double getWheelDia() {
					return 5.896437 * 0.86;
				}
		};

//		} else {
//			instance = null;
//			throw new RuntimeException("Unknown robot");
//		}

	}
	public static RobotConfigurationIF getInstance() {
		return instance;
	}

	public static String getRobotMacAddress() {
		String macAdress = null;
		InetAddress ip;
		NetworkInterface network;
		//Get mac Address in bytes
		byte[] mac = null;
		try {
			ip = InetAddress.getLocalHost();
			network = NetworkInterface.getByInetAddress(ip);
			mac = network.getHardwareAddress();
		} catch (SocketException | UnknownHostException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Error Generating Mac Address", e);
		}
		//Generate String
		for(byte b : mac) {
			macAdress += b;
		}
		return macAdress;

	}

}
