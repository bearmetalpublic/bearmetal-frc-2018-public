/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.autonomous.Autonomous;
import org.tahomarobotics.robot.arm.Arm;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.collector.Collector;
import org.tahomarobotics.robot.lift.Lift;
import org.tahomarobotics.robot.state.FieldData;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.RobotLogger;
import org.tahomarobotics.robot.util.UpdaterIF;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Robot extends TimedRobot {

	private static final Logger logger = Logger.getLogger(Robot.class.getSimpleName());

	private static final double ROBOT_PERIOD = 0.010;

	// this holds all of the subsystems and other singleton instances
	private final List<UpdaterIF> updater = new ArrayList<>();
	
	private double time = Timer.getFPGATimestamp();
	public Robot() {
		RobotLogger.initialize();
		System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tl:%1$tM:%1$tS.%1$tL %4$s : %5$s%n");// -
																												// %2$s
		try {
			/*
			 * Add instances of all required components
			 * The try-catch will print any initialization errors
			 */
			updater.add(OI.getInstance());
			updater.add(Chassis.getInstance());
			updater.add(RobotState.getInstance());
			updater.add(FieldData.getInstance());
			updater.add(Autonomous.getInstance());
			updater.add(Lift.getInstance());
			updater.add(Collector.getInstance());
			updater.add(Arm.getInstance());
			
			setPeriod(ROBOT_PERIOD);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed creating Robot", e);
		}
	}

	@Override
	public void robotInit() {
		logger.log(Level.INFO, "Robot init");
		Autonomous.getInstance().isStarted = false;
		try {
			for(UpdaterIF update : updater) {
				update.init();
			}
		} catch (Throwable e) {

			e.printStackTrace();
			logger.log(Level.SEVERE, "Failed to init Robot", e);
			
			// halt by spinning if failed
			while (true) {
				// Wait for new data to arrive
				DriverStation.getInstance().waitForData();
			}
		}
	}

	
	/**
	 * Called every 10 milliseconds
	 */
	@Override
	public void robotPeriodic() {
		
		double start = Timer.getFPGATimestamp();
		
		for (UpdaterIF update : updater) {
			update.update();
		}

		// run the commands added to the scheduler
		Scheduler.getInstance().run();
		
		double end = Timer.getFPGATimestamp();
		
		double period = (start - time);
		SmartDashboard.putNumber("Robot Loading", 100 * (end-start)/period );
		SmartDashboard.putNumber("Robot Period", period );
		time = start;

	}

	@Override
	public void disabledInit() {
		FieldData.getInstance().getAlliance();
		RobotState.getInstance().setCalibration(true);
		logger.log(Level.INFO, "Disabled");
	}

	@Override
	public void disabledPeriodic() {
	}

	@Override
	public void autonomousInit() {
		FieldData.getInstance().resetSlugTrail();
		Arm.getInstance().forceCubeDetect();
		
		RobotState.getInstance().setCalibration(false);
		//Gets the field data at the very beginning of autonomous.
		//We could poll during disabled period although there may be remnant data from the previous match.
		FieldData.getInstance().getFieldDataFromDriverStation();
		Autonomous.getInstance().start();
		logger.log(Level.INFO, "Our Alliance Switch Plate is to the: " + FieldData.getInstance().getFieldColorConfig(FieldData.Field_Position.ALLIANCE_SWITCH));
		logger.log(Level.INFO, "The Scale Plate is to the: " + FieldData.getInstance().getFieldColorConfig(FieldData.Field_Position.SCALE));
		logger.log(Level.INFO, "Autonomous Started");
	}

	@Override
	public void autonomousPeriodic() {
		if(!Autonomous.getInstance().isStarted) {
			FieldData.getInstance().getFieldDataFromDriverStation();
			Autonomous.getInstance().start();
		}
	}

	@Override
	public void teleopInit() {
		Arm.getInstance().clearForceCubeDetect();
		Autonomous.getInstance().stop();
		logger.log(Level.INFO, "Tele-op Started");
		RobotState.getInstance().setCalibration(false);
	}

	@Override
	public void teleopPeriodic() {
	}
}
