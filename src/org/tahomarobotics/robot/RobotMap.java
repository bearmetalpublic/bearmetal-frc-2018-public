/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */

package org.tahomarobotics.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	public static final int DRIVE_SOLENOID = 0;
	
	public static final int LIFT_SOLENOID = 1;
	
	public static final int PCM_MODULE = 0;
	
	public static final int PIGEON = 7;

	public static final int LEFT_FRONT_MOTOR = 5;
	public static final int LEFT_MIDDLE_MOTOR = 4;
	public static final int LEFT_BACK_MOTOR = 6;
	
	public static final int RIGHT_FRONT_MOTOR = 2;
	public static final int RIGHT_MIDDLE_MOTOR = 1;
	public static final int RIGHT_BACK_MOTOR = 0;
	
	public static final int LIFT_MIDDLE_MOTOR = 7;
	public static final int LIFT_BOTTOM_MOTOR = 8;
	public static final int LIFT_TOP_MOTOR = 3;
	
	public static final int ARM_MOTOR = 9;

	public static final int COLLECTOR_LEFT_MOTOR = 10;
	public static final int COLLECTOR_RIGHT_MOTOR = 11;
	
	public static final int COLLECTOR_RIGHT_LIGHT_SWITCH = 1;
}
