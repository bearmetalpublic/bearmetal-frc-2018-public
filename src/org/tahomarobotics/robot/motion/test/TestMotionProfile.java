/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.motion.test;

import java.util.HashMap;
import java.util.Map;

import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions;
import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestMotionProfile extends Application {

	private static final double PERCENT = 1;
	private static final double TRAVEL = 60 * PERCENT;
	private static final double MAX_VELOCITY = 95;
	private static final double MAX_ACCEL = 1000;
	private static final double MAX_JERK = 3000;

	private final Map<String, XYChart.Series<Number,Number>> series = new HashMap<>();
	
	private void addData(String name, double time, double value) {
		XYChart.Series<Number,Number> data = series.get(name);
		if (data == null) {
			data = new XYChart.Series<>();
			data.setName(name);
			series.put(name, data);
		}
		data.getData().add(new XYChart.Data<Number,Number>(time, value));
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Conditions k = new Conditions(Profile.BetterSCurve, 0, TRAVEL, 0, 0, MAX_VELOCITY, MAX_ACCEL, MAX_JERK);
		MotionProfile profile = new MotionProfile(0, k);

		double endTime = profile.getEndTime();
		final MotionState setpoint = new MotionState();
		
		double yMax = 0;
		double yMin = 0;
		
		for (double time = 0; time <= endTime; time += 0.001) {
			boolean success = profile.getSetpoint(time, setpoint);
			if (success) {
				yMax = Math.max(yMax, setpoint.position);
				addData("Position (cmd)", time, setpoint.position);
				yMax = Math.max(yMax, setpoint.velocity);
				addData("Velocity (cmd)", time, setpoint.velocity);
				
//				System.out.format("%6.3fs %6.3fin %6.3fin/s %6.3fin/s2\n", 
//						time, setpoint.position, setpoint.velocity, setpoint.acceleration);
			}
		}
		
		double xMin = 0;
		double xMax = endTime;
		yMax *= 1.1;
		yMin = 0;
		
		primaryStage.setTitle("Lift Motion");
        final NumberAxis xAxis = new NumberAxis(xMin, xMax, 0.1);
        final NumberAxis yAxis = new NumberAxis(yMin, yMax, 5);        
        final LineChart<Number,Number> sc = new LineChart<>(xAxis,yAxis);
        xAxis.setLabel("Time (sec)");                
        yAxis.setLabel("Velocity (inch/sec)");
        sc.setTitle("Lift Motion");

        sc.setPrefSize(1280, 1024);
        sc.getData().addAll(series.values());
        Scene scene  = new Scene(new Group());
        final VBox vbox = new VBox();
        final HBox hbox = new HBox();
               
        hbox.setSpacing(10);
        
        vbox.getChildren().addAll(sc);
        hbox.setPadding(new Insets(10, 10, 10, 50));
        
        ((Group)scene.getRoot()).getChildren().add(vbox);
        
        scene.getStylesheets().add(getClass().getResource("chart.css").toExternalForm());
        
        primaryStage.setScene(scene);
        primaryStage.show();
		
	}

	public static void main(String[] args) {
        launch(args);
	}
}
