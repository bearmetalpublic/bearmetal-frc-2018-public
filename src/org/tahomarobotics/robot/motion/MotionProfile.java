/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.motion;

import java.util.ArrayList;
import java.util.List;

import org.tahomarobotics.robot.motion.MotionProfile.Conditions.Profile;

public class MotionProfile {
	
	@SuppressWarnings("serial")
	public class MotionProfileException extends Exception {

		public MotionProfileException(String message) {
			super(message);
		}
		
	}
	
	public static class MotionSection {
		public final double length;
		public final double maxVelocity;
		
		public MotionSection(double length, double maxVelocity) {
			this.length = length;
			this.maxVelocity = maxVelocity;
		}

		@Override
		public String toString() {
			return String.format("%5.1f %3.0f", length, maxVelocity);
		}
		
	}

	public static class Conditions {
		
		public enum Profile {
			Trapazoid, SCurve, BetterSCurve;
		}
		
		public final double startPosition;
		public final double endPosition;
		public final double startVelocity;
		public final double endingVelocity;
		public final double maxVelocity;
		public final double maxAcceleration;
		public final double maxJerk;
		public final Profile profile;
		
		public Conditions(final Profile profile, final double startPosition, final double endPosition, 
				final double startVelocity, final double endingVelocity, 
				final double maxVelocity, final double maxAcceleration, final double maxJerk) {
			this.startPosition = startPosition;
			this.endPosition = endPosition; 
			this.startVelocity = startVelocity;
			this.endingVelocity = endingVelocity; 
			this.maxVelocity = maxVelocity;
			this.maxAcceleration = maxAcceleration;
			this.maxJerk = maxJerk;
			this.profile = profile;
		}
	}
		
	private final MotionState phases[];
		
	public MotionProfile(final double startTime, final Conditions k) throws MotionProfileException {
		
		switch(k.profile) {
		
		case Trapazoid:
			phases = geTrapezoidalProfile(startTime, k);
			break;
			
		case SCurve:
			phases = getSCurveProfile(startTime, k);
			break;
			
		case BetterSCurve:
			phases = getBetterSCurveProfile(startTime, k);
			break;
		
		default:
			phases = null;
		}
	}
	
	/**
	 * With the provided constraints, a three phase trapezoidal motion profile
	 * @throws MotionProfileException 
	 * 
	 */
	private MotionState[] geTrapezoidalProfile(final double startTime, final Conditions k) throws MotionProfileException {
		
		final double distance = k.endPosition - k.startPosition;
		
		final double max_velocity = Math.min(
				k.maxVelocity,
				Math.sqrt(distance*k.maxAcceleration + k.startVelocity * k.startVelocity / 2 
						+ k.endingVelocity * k.endingVelocity / 2));
		
		final double ta = (max_velocity - k.startVelocity) / k.maxAcceleration;
		final double td = (max_velocity - k.endingVelocity) / k.maxAcceleration;
		final double tv = (distance - 0.5*ta*(max_velocity+k.startVelocity) - 0.5*td*(max_velocity+k.endingVelocity)) / max_velocity;
	
		if (ta < 0 || td < 0 || tv < 0) {
			throw new MotionProfileException(String.format("Failed to resolve constraint while creating Trapezoidal profile ta=%f td=%f tv=%f", ta, td, tv));
		}
		
		MotionState phases[] = new MotionState[4];

		// initial state
		MotionState initial = phases[0] = new MotionState()
				.setTime(startTime)
				.setPosition(k.startPosition)
				.setVelocity(k.startVelocity)
				.setAcceleration(k.maxAcceleration);
		
		// end of constant acceleration
		initial = phases[1] = getPhaseSetpoint(ta, initial, new MotionState())
				.setVelocity(max_velocity)
				.setAcceleration(0);
		
		// end of constant velocity
		initial = phases[2] = getPhaseSetpoint(tv, initial, new MotionState())
				.setAcceleration(-k.maxAcceleration);

		// end of constant deceleration
		initial = phases[3] = getPhaseSetpoint(td, initial, new MotionState())
				.setVelocity(k.endingVelocity)
				.setPosition(k.endPosition)
				.setAcceleration(0);

		return phases;
	}

	
	private MotionState[] getSCurveProfile(final double startTime, final Conditions k) {
		
		final double distance = k.endPosition - k.startPosition;

		// take the lower velocity of the constraint and peak velocity
		final double maxVelocity = Math.min(k.maxVelocity, Math.sqrt((distance*k.maxAcceleration + k.startVelocity * k.startVelocity + k.endingVelocity * k.endingVelocity)/2));
		
		// acceleration time which include acceleration ramp up, constant and ramp down
		// estimate using trapezoid ramping at half the acceleration
		// this results in lower peak acceleration
		// also to ensure acceleration time is large enough for small velocity transition it take the larger
		// time needed to limit jerk
		final double rampAccel =  k.maxAcceleration / 2;
		final double ta = Math.max(
				(maxVelocity - k.startVelocity)/rampAccel, 
				2*Math.sqrt((maxVelocity - k.startVelocity)/k.maxJerk));
		final double td = Math.max(
				(maxVelocity - k.endingVelocity)/rampAccel, 
				2*Math.sqrt((maxVelocity - k.endingVelocity)/k.maxJerk));
		
		// time for constant velocity
		final double tv = Math.max(0, (distance + k.startVelocity * ta / 2 - k.endingVelocity * td / 2) / maxVelocity - ta/2 -td/2);

		// jerk time
		final double tja = ta/2 - Math.sqrt(Math.max(0,ta*ta-4*(maxVelocity - k.startVelocity)/k.maxJerk))/2;
		final double tjd = td/2 - Math.sqrt(Math.max(0,td*td-4*(maxVelocity - k.endingVelocity)/k.maxJerk))/2;
		
		// peak acceleration
		final double accel = k.maxJerk * tja;
		final double decel = k.maxJerk * tjd;
		
		
		MotionState phases[] = new MotionState[8];
		
		// start
		phases[0] = new MotionState();
		phases[0].time         = startTime;
		phases[0].jerk         = k.maxJerk;
		phases[0].acceleration = 0;
		phases[0].velocity     = k.startVelocity;
		phases[0].position     = k.startPosition;
		MotionState initial = phases[0];
		
		// end of phase 1 - acceleration ramp up
		initial = phases[1] = getPhaseSetpoint(tja, initial, new MotionState()).setJerk(0).setAcceleration(accel);
		
		// end of phase 2 - constant acceleration
		initial = phases[2] = getPhaseSetpoint(ta - 2*tja, initial, new MotionState()).setJerk(-k.maxJerk);
		
		// end of phase 3 - acceleration ramp down
		initial = phases[3] = getPhaseSetpoint(tja, initial, new MotionState()).setJerk(0).setAcceleration(0);

		// end of phase 4 - constant velocity
		initial = phases[4] = getPhaseSetpoint(tv, initial, new MotionState()).setJerk(-k.maxJerk);

		// end of phase 5 - deceleration ramp up
		initial = phases[5] = getPhaseSetpoint(tjd, initial, new MotionState()).setJerk(0).setAcceleration(-decel);

		// end of phase 6 - constant deceleration
		initial = phases[6] = getPhaseSetpoint(td - 2*tjd, initial, new MotionState()).setJerk(k.maxJerk);

		// end of phase 7 - deceleration ramp down
		initial = phases[7] = getPhaseSetpoint(tjd, initial, new MotionState()).setJerk(0).setAcceleration(0);

		return phases;
}
	
	private MotionState[] getBetterSCurveProfile(final double startTime, final Conditions k) throws MotionProfileException {
		double difference = k.endPosition - k.startPosition;
		final double direction = Math.signum(difference);
		final double distance = Math.abs(difference);
		double maxVelocity = k.maxVelocity;
		double maxAccel = k.maxAcceleration;
		double maxJerk = k.maxJerk;
		double tv = distance / maxVelocity;
		double ta = 0.20;
		double tj = 0.1;
		
		for (int i = 0; i < 10; i++) {
			maxVelocity = Math.min(maxVelocity,
				(-maxAccel * maxAccel
						+ Math.sqrt(maxAccel * maxAccel * maxAccel * maxAccel
								+ 4 * maxJerk * maxJerk * maxAccel * distance)) / (2 * maxJerk));
		
			tv = distance / maxVelocity;
			maxAccel = maxVelocity / ta;
			maxJerk = maxAccel / tj;
			
			if (Double.isNaN(tv) || tv < 0 || Double.isNaN(maxAccel) || maxAccel <= 0 || Double.isNaN(maxJerk) || maxJerk < 0) {
				throw new MotionProfileException(String.format("Failed to resolve constraint while creating BetterSCurve profile %6.3f %6.3f %6.3f %6.3f", maxVelocity, tv, maxAccel, maxJerk));
			}
		}
		maxVelocity *= direction;
		maxAccel *= direction;
		maxJerk *= direction;
		MotionState phases[] = new MotionState[8];
		
		// start
		phases[0] = new MotionState();
		phases[0].time         = startTime;
		phases[0].jerk         = maxJerk;
		phases[0].acceleration = 0;
		phases[0].velocity     = 0;
		phases[0].position     = k.startPosition;
		MotionState initial = phases[0];
		
		// end of phase 1 - acceleration ramp up
		initial = phases[1] = getPhaseSetpoint(tj, initial, new MotionState()).setJerk(0).setAcceleration(maxAccel);
		
		// end of phase 2 - constant acceleration
		initial = phases[2] = getPhaseSetpoint(ta - tj, initial, new MotionState()).setJerk(-maxJerk);
		
		// end of phase 3 - acceleration ramp down
		initial = phases[3] = getPhaseSetpoint(tj, initial, new MotionState()).setJerk(0).setAcceleration(0);

		// end of phase 4 - constant velocity
		initial = phases[4] = getPhaseSetpoint(tv - ta - tj, initial, new MotionState()).setJerk(-maxJerk);

		// end of phase 5 - deceleration ramp up
		initial = phases[5] = getPhaseSetpoint(tj, initial, new MotionState()).setJerk(0).setAcceleration(-maxAccel);

		// end of phase 6 - constant deceleration
		initial = phases[6] = getPhaseSetpoint(ta - tj, initial, new MotionState()).setJerk(maxJerk);

		// end of phase 7 - deceleration ramp down
		initial = phases[7] = getPhaseSetpoint(tj, initial, new MotionState()).setJerk(0).setAcceleration(0);

		return phases;
	}
	
	public boolean getSetpoint(final double time, final MotionState setpoint) {
		
		for (int i = 1; i < phases.length; i++) {
			if (time < phases[i].time) {
				MotionState initial = phases[i-1];
				getPhaseSetpoint(time - initial.time, initial, setpoint);
				return true;
			}
		}
		
		// copy end set point
		setpoint.copy(phases[phases.length-1]);
		
		return false;
	}

	private MotionState getPhaseSetpoint(final double dt, final MotionState initial, MotionState setpoint) {
		if (setpoint == null) {
			setpoint = new MotionState();
		}
		double jt = initial.jerk * dt;
		double jt2 = jt * dt / 2;
		double jt3 = jt2 * dt / 3;
		double at = initial.acceleration * dt;
		double at2 = at * dt / 2;
		double vt = initial.velocity * dt;
		
		setpoint.time         = initial.time + dt;
		setpoint.jerk         = initial.jerk;
		setpoint.acceleration = initial.acceleration + jt;
		setpoint.velocity     = initial.velocity + at + jt2;
		setpoint.position     = initial.position + vt + at2 + jt3;
		return setpoint;
	}
	
	public double getEndTime() {
		return phases[phases.length - 1].time;
	}
	
	public double getEndPosition() {
		return phases[phases.length - 1].position;
	}
	
	public static List<MotionSection> combine(List<MotionSection> sections) {
		
		// combine sections if possible
		double len = 0;
		double max = sections.get(0).maxVelocity;
		List<MotionSection> combined = new ArrayList<>();
		for(MotionSection section : sections) {
			if (max != section.maxVelocity) {
				combined.add(new MotionSection(len, max));	
				len = 0;
			}
			max = section.maxVelocity;
			len += section.length;
		}
		combined.add(new MotionSection(len, max));		

		return combined;
	}
	
	public static MotionProfile[] createProfles(final Profile type, final double maxAcceleration, final double maxJerk, List<MotionSection> sections) throws MotionProfileException {

		List<MotionProfile> profiles = new ArrayList<>();

		double startTime = 0;
		double startVelocity = 0;
		double startPosition = 0;
		
		for(int i = 0; i < sections.size(); i++) {
			MotionSection section = sections.get(i);
			
			double endPosition = startPosition + section.length;
			double maxVelocity = section.maxVelocity;
			double nextVelocity = (i+1) < sections.size() ? sections.get(i+1).maxVelocity : 0; 
			double endVelocity = Math.min(maxVelocity, nextVelocity);
			
			Conditions k = new Conditions(type, startPosition, endPosition, startVelocity, endVelocity, maxVelocity, maxAcceleration, maxJerk);
			MotionProfile profile = new MotionProfile(startTime, k);
			profiles.add(profile);
			
			startTime = profile.getEndTime();
			startVelocity = endVelocity;
			startPosition = endPosition;
		}
		
		return profiles.toArray(new MotionProfile[profiles.size()]);
	}
	

}
