/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import org.tahomarobotics.robot.arm.PowerArm;
import org.tahomarobotics.robot.auto.ClimbMovements;
import org.tahomarobotics.robot.auto.CollectPosition;
import org.tahomarobotics.robot.auto.MaximumScalePosition;
import org.tahomarobotics.robot.auto.MinimumScalePosition;
import org.tahomarobotics.robot.auto.ReverseScalePosition;
import org.tahomarobotics.robot.auto.StowPosition;
import org.tahomarobotics.robot.auto.ToProtectMode;
import org.tahomarobotics.robot.chassis.Chassis.Gear;
import org.tahomarobotics.robot.chassis.Shift;
import org.tahomarobotics.robot.lift.ShiftLift;
import org.tahomarobotics.robot.state.RobotState.ResetRobotState;
import org.tahomarobotics.robot.util.ToggleCommand;
import org.tahomarobotics.robot.util.UpdaterIF;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Operator Interface, OI, is used to connect the robot to the controls of the Driver 
 * Station where the Drive Team operates the controls.  It should connect robot command
 * to various controller buttons, sticks and triggers.  Additional controllers can be 
 * added if needed. 
 *
 */
@SuppressWarnings("unused")
public class OI implements UpdaterIF {

	// single instance of OI
	private static final OI instance = new OI();

	// hand controller
	private final XboxController driverController = new XboxController(0);
	private final XboxController manipulatorController = new XboxController(1);
	
	private static final double DEAD_BAND = 12 / 127.0;

	// event buttons
	private final JoystickButton driverButtonA = new JoystickButton(driverController, 1);
	private final JoystickButton driverButtonB = new JoystickButton(driverController, 2);
	private final JoystickButton driverButtonX = new JoystickButton(driverController, 3);
	private final JoystickButton driverButtonY = new JoystickButton(driverController, 4);
	private final JoystickButton driverButtonRBumber = new JoystickButton(driverController, 6);
	private final JoystickButton driverButtonLBumber = new JoystickButton(driverController, 5);
	private final JoystickButton driverButtonBack = new JoystickButton(driverController, 7);
	private final JoystickButton driverButtonStart = new JoystickButton(driverController, 8);

	private final JoystickButton manipulatorButtonX = new JoystickButton(manipulatorController, 3);
	private final JoystickButton manipulatorButtonY = new JoystickButton(manipulatorController, 4);
	private final JoystickButton manipulatorButtonA = new JoystickButton(manipulatorController, 1);
	private final JoystickButton manipulatorButtonB = new JoystickButton(manipulatorController, 2);
	private final JoystickButton manipulatorButtonLBumber = new JoystickButton(manipulatorController, 5);
	private final JoystickButton manipulatorButtonBack = new JoystickButton(manipulatorController, 7);
	private final JoystickButton manipulatorButtonStart = new JoystickButton(manipulatorController, 8);

	private boolean isDriverLeftRumbleActive, isDriverRightRumbleActive;
	private boolean isManipulatorLeftRumbleActive, isManipulatorRightRumbleActive;

	private double driverLeftRumbleExpire, driverRightRumbleExpire;
	private double manipulatorLeftRumbleExpire, manipulatorRightRumbleExpire;
	
	private boolean buttonAToggle = true;
	
	private ToggleCommand collectPositionToggle = new ToggleCommand(new CollectPosition(), new StowPosition());
	
	/**
	 *  default constructor, this is private ensuring there is only 
	 *  one instance as defined above
	 */
	private OI() {
		driverButtonA.whenPressed(collectPositionToggle);
		driverButtonX.whenPressed(new CommandGroup() {
			{
				addSequential(collectPositionToggle.new ForceFirstCommand(collectPositionToggle));
				addSequential(new MinimumScalePosition());		
			}
		});
		driverButtonRBumber.whenPressed(new Shift(Gear.LOW));
		driverButtonLBumber.whenPressed(new Shift(Gear.HIGH));
		driverButtonBack.whenPressed(new ToProtectMode());
		driverButtonStart.whenPressed(new ClimbMovements());
		manipulatorButtonLBumber.whenPressed(new ShiftLift());
		manipulatorButtonA.whenPressed(collectPositionToggle);
		manipulatorButtonB.whenPressed(new CommandGroup() {
			{
				addSequential(collectPositionToggle.new ForceFirstCommand(collectPositionToggle));
				addSequential(new ReverseScalePosition());		
			}
		});
		manipulatorButtonX.whenPressed(new CommandGroup() {
			{
				addSequential(collectPositionToggle.new ForceFirstCommand(collectPositionToggle));
				addSequential(new MinimumScalePosition());		
			}
		});
		manipulatorButtonY.whenPressed(new CommandGroup() {
			{
				addSequential(collectPositionToggle.new ForceFirstCommand(collectPositionToggle));
				addSequential(new MaximumScalePosition());		
			}
		});		manipulatorButtonBack.whenPressed(new ToProtectMode());
		manipulatorButtonStart.whenPressed(new ClimbMovements());
		driverButtonB.whileHeld(new PowerArm(-0.5));
	}

	/**
	 * Returns the one instance of OI
	 */
	public static OI getInstance() {
		return instance;
	}

	public double getDriverLeftYAxis() {
		return -applyDeadBand(driverController.getY(Hand.kLeft));
	}

	public double getDriverRightYAxis() {
		return -applyDeadBand(driverController.getY(Hand.kRight));
	}

	public double getManipulatorLeftYAxis() {
		return -manipulatorController.getY(Hand.kLeft);
	}

	public double getManipulatorRightYAxis() {
		return -manipulatorController.getY(Hand.kRight);
	}
	
	public double getManipulatorLeftTrigger() {
		return manipulatorController.getTriggerAxis(Hand.kLeft);
	}
	
	public double getManipulatorRightTrigger() {
		return manipulatorController.getTriggerAxis(Hand.kRight);
	}
	
	public void startManipulatorLeftRumble(double seconds) {
		manipulatorController.setRumble(RumbleType.kLeftRumble, 1);
		isManipulatorLeftRumbleActive = true;
		manipulatorLeftRumbleExpire = System.currentTimeMillis() + (seconds * 1000);
	}
	
	public void startManipulatorRightRumble(double seconds) {
		manipulatorController.setRumble(RumbleType.kRightRumble, 1);
		isManipulatorRightRumbleActive = true;
		manipulatorRightRumbleExpire = System.currentTimeMillis() + (seconds * 1000);
	}
	
	public void startDriverLeftRumble(double seconds) {
		driverController.setRumble(RumbleType.kLeftRumble, 1);
		isDriverLeftRumbleActive = true;
		driverLeftRumbleExpire = System.currentTimeMillis() + (seconds * 1000);
	}
	
	public void startDriverRightRumble(double seconds) {
		driverController.setRumble(RumbleType.kRightRumble, 1);
		isDriverRightRumbleActive = true;
		driverRightRumbleExpire = System.currentTimeMillis() + (seconds * 1000);
	}

	/**
	 * Removes small values emitted from the controller axis when stick is relaxed.
	 * 
	 * Returns the value only if value isn't in the range of -DEAD_BAND to +DEAD_BAND
	 */
	private double applyDeadBand(double value){
		return applyDeadBand(value, DEAD_BAND);
	}

	public static double applyDeadBand(double value, double deadBand) {
		if (Math.abs(value) < deadBand) {
			return 0.0;
		}
		return (Math.abs(value) - deadBand) / (1 - deadBand) * Math.signum(value);
	}

	@Override
	public void update() {
		if (isDriverLeftRumbleActive && (driverLeftRumbleExpire < System.currentTimeMillis())) {
			driverController.setRumble(RumbleType.kLeftRumble, 0);
			isDriverLeftRumbleActive = false;
		}

		if (isDriverRightRumbleActive && (driverRightRumbleExpire < System.currentTimeMillis())) {
			driverController.setRumble(RumbleType.kRightRumble, 0);
			isDriverRightRumbleActive = false;
		}
		
		if (isManipulatorLeftRumbleActive && (manipulatorLeftRumbleExpire < System.currentTimeMillis())) {
			manipulatorController.setRumble(RumbleType.kLeftRumble, 0);
			isManipulatorLeftRumbleActive = false;
		}

		if (isManipulatorRightRumbleActive && (manipulatorRightRumbleExpire < System.currentTimeMillis())) {
			manipulatorController.setRumble(RumbleType.kRightRumble, 0);
			isManipulatorRightRumbleActive = false;
		}
	}

	public double getDriverLeftTrigger() {
		return driverController.getTriggerAxis(Hand.kLeft);
	}
	
	public double getDriverRightTrigger() {
		return driverController.getTriggerAxis(Hand.kRight);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	public int getDriverPOV() {
		return driverController.getPOV();
	}

}
