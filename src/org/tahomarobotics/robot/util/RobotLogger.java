/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.DriverStation;

public class RobotLogger extends Formatter {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	
	public static void initialize() {
		Logger root = Logger.getLogger("");
		for (Handler handler : root.getHandlers()) {
			root.removeHandler(handler);
		}
		Handler handler = new ConsoleHandler();
		handler.setFormatter(new RobotLogger());
		root.addHandler(handler);
	}
	
	@Override
	public String format(LogRecord record) {
		
		
		StringBuffer sb = new StringBuffer();
		sb.append(record.getMillis());

		String message = String.format("%s %s [%s] %s -> %s\n",
				dateFormat.format(new Date(record.getMillis())),
				record.getLevel().getName(),
				Thread.currentThread().getName(),
				record.getLoggerName(),
				record.getMessage());
		
		Level level = record.getLevel();
		
		if (level == Level.SEVERE) {
			DriverStation.reportError(message, false);
		} else if (level == Level.WARNING) {
			DriverStation.reportWarning(message, false);
		}
		
		return message;
	}
}
