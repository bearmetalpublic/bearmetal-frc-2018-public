/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.state;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.util.UpdaterIF;

import com.ctre.phoenix.sensors.PigeonIMU;
import com.ctre.phoenix.sensors.PigeonIMU.CalibrationMode;
import com.ctre.phoenix.sensors.PigeonIMU.PigeonState;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

@SuppressWarnings("unused")
public class RobotState implements UpdaterIF {

	private static final Logger logger = Logger.getLogger(RobotState.class.getSimpleName());
	
	public enum RotationMode{
		IMU_ONLY, BLENDED;
	}
	
	private static final RobotState instance = new RobotState();

	private double x = 0;
	private double y = 0;
	private double heading = 0;
	private RobotSpeed speed = new RobotSpeed();
	private double acceleration;
	
	private long prevTime = System.currentTimeMillis();

	private final PigeonIMU imu;
	private final Chassis chassis;
	private final RobotStateEstimator estimator;
	
	private final int RUNNING_AVERAGE_MILLISECONDS = 100;
	
	private boolean isCalibrating = true;
	private double[] runningAverage = new double[RUNNING_AVERAGE_MILLISECONDS];
	private int runningAverageIndex = 0;
	
	public static RobotState getInstance() {
		return instance;
	}

	private RobotState() {
		imu = new PigeonIMU(RobotMap.PIGEON);
		chassis = Chassis.getInstance();
		estimator = new RobotStateEstimator(chassis, imu);
		setRotationMode(RotationMode.IMU_ONLY);
		//imu.calibrate();
	}

	public Pose2D getPose(Pose2D pose) {
		synchronized (instance) {
			pose.x = x;
			pose.y = y;
			pose.heading = heading;
			return pose;
		}
	}

	public void getSpeed(RobotSpeed speed) {
		synchronized (instance) {
			speed.forward = this.speed.forward;
			speed.rotational = this.speed.rotational;
		}
	}

	public void getState(State state) {
		getPose(state.pose);
		getSpeed(state.velocity);
		state.setAcceleration(acceleration);
	}
	
	private void setRobotState(double x, double y, double heading) {
		synchronized (instance) {
			this.x = x;
			this.y = y;
			this.heading = heading;
		}
	}
	
	void calibrateGyro() {
		imu.enterCalibrationMode(CalibrationMode.BootTareGyroAccel, 0);
	}
	
	boolean isFinishedCalibrating() {
		return imu.getState() == PigeonState.Ready;
	}

	private class RobotStateEstimator {
		private final static double FREQ_CUTOFF = 1.0; // Hz
		private final static double TIME_CONSTANT = 1.0 / (2.0 * Math.PI * FREQ_CUTOFF);
		private final Chassis chassis;
		public double yawRateOffset = 0;
		private RotationMode rotationMode;

		public RobotStateEstimator(Chassis chassis, PigeonIMU imu) {
			this.chassis = chassis;
		}

		private void updateRobotState(double dTime, RobotSpeed odometrySpeed, RobotSpeed imuSpeed) {
			// speed complementary filter
			double alpha = 1.0;
			switch(rotationMode){
			case BLENDED:
				alpha = dTime / (TIME_CONSTANT + dTime);
				break;
			case IMU_ONLY:
				alpha = 0.0;
				break;
			default:
				break;
			}
			
//			if(isCalibrating && Math.abs(odometrySpeed.forward) < 0.001) {
//				runningAverage[runningAverageIndex] = imuSpeed.rotational;
//				runningAverageIndex++;
//				if(runningAverageIndex > RUNNING_AVERAGE_MILLISECONDS - 1) {
//					runningAverageIndex = 0;
//				}
//				
//				double rotationalHeadingAverage = 0;
//				for(double x : runningAverage) {
//					rotationalHeadingAverage += x;
//				}
//				
//				yawRateOffset = 0;//rotationalHeadingAverage / RUNNING_AVERAGE_MILLISECONDS;
//			}
			
			double deltaHeading = complementryFilter(dTime * odometrySpeed.rotational,
					dTime * (imuSpeed.rotational), alpha);
			double blendedHeading = normalizeAngle(heading + deltaHeading);
			double rotationalSpeed = deltaHeading / dTime;

			// delta pose
			double dX = odometrySpeed.forward * dTime * Math.cos(heading);
			double dY = odometrySpeed.forward * dTime * Math.sin(heading);

			
			SmartDashboard.putNumber("Yaw Rate Offset", yawRateOffset);
			// update robot state
			double forward = odometrySpeed.forward;
			synchronized (instance) {
				acceleration = (forward - speed.forward)/dTime;
				speed.forward = forward;
				speed.rotational = rotationalSpeed;
				x = x + dX;
				y = y + dY;
				heading = blendedHeading;
			}
		}
		
		private void setRotationMode(RotationMode rotationMode){
			logger.log(Level.INFO, "Setting rotation mode to: " + rotationMode.toString());
			
			this.rotationMode = rotationMode;
		}
		
		private RotationMode getRotationMode(){
			return rotationMode;
		}

		public void update() {
			// delta time
			long time = System.currentTimeMillis();
			double dTime = (time - prevTime) * 0.001;
			if (dTime < 0.001) {
				return;
			}
			prevTime = time;

			// sensor inputs
			RobotSpeed odometrySpeed = chassis.getOdometrySpeed();
			RobotSpeed imuSpeed = getIMUSpeed(speed.forward, dTime);
				
			updateRobotState(dTime, odometrySpeed, imuSpeed);
		}
	}

	private double complementryFilter(double low, double high, double alpha) {
		return low * alpha + high * (1 - alpha);
	}

	private static final double TWO_PI = 2.0 * Math.PI;

	public static double normalizeAngle(double angle) {
		return (angle + TWO_PI) % TWO_PI;
	}

	private double newHeading = 0;
	private double oldHeading = 0;
	
	public RobotSpeed getIMUSpeed(double forwardSpeed, double dTime) {
		oldHeading = newHeading;
		newHeading = Math.toRadians(imu.getFusedHeading());
		double rotationalSpeed = (newHeading - oldHeading) / dTime;
		rotationalSpeed = Math.signum(rotationalSpeed) * Math.min(Math.abs(rotationalSpeed), 10.0);

		return new RobotSpeed(forwardSpeed, rotationalSpeed);
	}
	
	public void setCalibration(boolean calibrate) {
		isCalibrating = calibrate;
	}
	
	public RotationMode getRotationMode(){
		return estimator.getRotationMode();
	}
	
	public void setRotationMode(RotationMode rotationMode){
		estimator.setRotationMode(rotationMode);
	}

	public static class ResetRobotState extends Command {

		private final double x, y, heading;

		public ResetRobotState() {
			this(0, 0, 0);
		}

		public ResetRobotState(double x, double y, double heading) {
			this.setRunWhenDisabled(true);
			this.x = x;
			this.y = y;
			this.heading = heading;
		}

		public ResetRobotState(Pose2D pose) {
			this(pose.x, pose.y, pose.heading);
		}
		
		@Override
		protected boolean isFinished() {
			RobotState.getInstance().setRobotState(x, y, heading);
			return true;
		}

	}

	private Pose2D distancePose = new Pose2D(0, 0, 0);

	public double getDistance(Pose2D other) {
		Pose2D currentPose = getPose(distancePose);
		double dX = currentPose.x - other.x;
		double dY = currentPose.y - other.y;
		return Math.sqrt(dX * dX + dY * dY);
	}

	private Pose2D smartDashboardPose = new Pose2D(0, 0, 0);

	@Override
	public void update() {
		estimator.update();
		getPose(smartDashboardPose);
		SmartDashboard.putNumber("Robot Heading", smartDashboardPose.heading);
		SmartDashboard.putNumber("Robot X", smartDashboardPose.x);
		SmartDashboard.putNumber("Robot Y", smartDashboardPose.y);
		SmartDashboard.putNumber("Fused heading", imu.getFusedHeading());
	}

	@Override
	public void init() {
		imu.setFusedHeading(0, 0);
		SmartDashboard.setDefaultNumber("Set Robot X", 0);
		SmartDashboard.setDefaultNumber("Set Robot Y", 0);
		SmartDashboard.setDefaultNumber("Set Robot Heading", 0);
		SmartDashboard.putData("Calibrate Gyro", new CalibrateGyro());
		SmartDashboard.putData("Set Conditions", new ResetRobotState(
				SmartDashboard.getNumber("Set Robot X", 0),
				SmartDashboard.getNumber("Set Robot Y", 0),
				SmartDashboard.getNumber("Set Robot Heading", 0)));
	}

	public void setRobotPose(Pose2D pose) {
		this.setRobotState(pose.x, pose.y, pose.heading);
	}

}
