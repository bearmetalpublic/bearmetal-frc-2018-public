/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.state;

import java.util.ArrayList;

import org.tahomarobotics.robot.path.PathCommand;
import org.tahomarobotics.robot.util.UpdaterIF;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class FieldData implements UpdaterIF {

	public enum Field_Position{
		ALLIANCE_SWITCH,
		SCALE,
		OPPONENT_SWITCH
	}
	
	public ArrayList<PathCommand> displayPaths = new ArrayList<PathCommand>();
	
	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("FieldData");
	private RobotState state = RobotState.getInstance();

	private static FieldData instance = new FieldData();
	
	private String switchScaleConfig;
	private Alliance alliance;
	
	public static FieldData getInstance() {
		return instance;
	}
	
	@Override
	public void init() {
		resetSlugTrail();
		SmartDashboard.putData("Reset Slug Trail", new Command() {
			{
				this.setRunWhenDisabled(true);
			}
			@Override
			protected boolean isFinished() {
				resetSlugTrail();
				return true;
			}
		});
	}

	private Pose2D fieldDataPose = new Pose2D(0, 0, 0);
	
	@Override
	public void update() {
		state.getPose(fieldDataPose);
		table.getEntry("Robot X").setNumber(fieldDataPose.x);
		table.getEntry("Robot Y").setNumber(fieldDataPose.y);
		table.getEntry("Robot Heading").setNumber(fieldDataPose.heading);
		
		for (int i = 0; i < displayPaths.size(); i++) {
			table.getEntry("Robot Path " + i).setString(displayPaths.get(i).serialize());
		}
		
		
		SmartDashboard.putBoolean("SlugTrailResetFlag", table.getEntry("SlugTrailResetFlag").getBoolean(false));
	}
	
	public void getFieldDataFromDriverStation() {
		/*
		 * Field Data (POV from Driver Station):
		 * Char 0: Closest Switch (Alliance)
		 * Char 1: Scale
		 * Char 2: Farthest Switch
		 */
		switchScaleConfig = DriverStation.getInstance().getGameSpecificMessage();
		alliance = DriverStation.getInstance().getAlliance();
		table.getEntry("Alliance").setString(alliance.toString());
		table.getEntry("Color String").setString(switchScaleConfig);
	}
	
	public Alliance getAlliance() {
		return DriverStation.getInstance().getAlliance();
	}
	
	public String getFieldColorConfig(Field_Position pos){
		try{
			switch(pos){
				case ALLIANCE_SWITCH:
					return Character.toString(switchScaleConfig.charAt(0));
				case SCALE:
					return Character.toString(switchScaleConfig.charAt(1));
				case OPPONENT_SWITCH:
					return Character.toString(switchScaleConfig.charAt(2));
			}
		}catch(Exception e){
			return "error with reciving field data";
		}

		throw new NullPointerException(); //This should never be reached to be honest.
									// If it does...I think a Null Pointer Exception is really in order...
	}
	
	public void resetSlugTrail() {
		table.getEntry("SlugTrailResetFlag").setBoolean(true);
	}
	
	public void clearPathDisplay() {
		for (int i = 0; i < displayPaths.size(); i++) {
			table.getEntry("Robot Path " + i).setString("Path{}");
		}
		displayPaths.clear();
	}
	
	public void addPath(PathCommand path) {
		displayPaths.add(path);
	}
}
