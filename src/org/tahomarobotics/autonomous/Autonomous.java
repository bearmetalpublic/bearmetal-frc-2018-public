/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.autonomous;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.auto.NoOp;
import org.tahomarobotics.robot.auto.groups.FarScaleFarSwitchGroup;
import org.tahomarobotics.robot.auto.groups.FarScaleGroup;
import org.tahomarobotics.robot.auto.groups.FarScaleNearSwitchGroup;
import org.tahomarobotics.robot.auto.groups.FarScalePlusGroup;
import org.tahomarobotics.robot.auto.groups.NearScaleFarSwitchGroup;
import org.tahomarobotics.robot.auto.groups.NearScaleGroup;
import org.tahomarobotics.robot.auto.groups.NearScaleNearSwitchGroup;
import org.tahomarobotics.robot.auto.groups.NearScalePlusGroup;
import org.tahomarobotics.robot.auto.groups.PathGroup;
import org.tahomarobotics.robot.auto.groups.SwitchFirstThenScaleOrSwitchGroup;
import org.tahomarobotics.robot.auto.groups.SwitchFirstThenScaleOrSwitchGroup.Side;
import org.tahomarobotics.robot.auto.groups.SwitchFirstThenScaleOrSwitchGroup.Type;
import org.tahomarobotics.robot.auto.paths.DriveForward;
import org.tahomarobotics.robot.auto.paths.PathConstants;
import org.tahomarobotics.robot.lift.Lift;
import org.tahomarobotics.robot.lift.Lift.LiftGear;
import org.tahomarobotics.robot.path.PathCommand;
import org.tahomarobotics.robot.path.PathCommand.RobotPosition;
import org.tahomarobotics.robot.state.FieldData;
import org.tahomarobotics.robot.state.FieldData.Field_Position;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.UpdaterIF;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Autonomous is used to manage the selection of the commands and command groups for
 * the autonomous phase of the robot.  The Drive Team will need to select the command
 * from one or more Smart Dash-board choosers (pick list).
 * 
 * Once selected on the driver station, autonomous initialization will call start running
 * the selected command(s).
 * 
 * It is importance to stop this command at the end of autonomous just in chase it is still
 * running.  Stop is called teleop initialization to do this.
 */
public class Autonomous implements UpdaterIF {

	private enum DriveSelections {
		DRIVE_STRAIGHT,
		SCALE,
		DOUBLE_SCALE,
		SCALE_SWITCH;
	}

	private enum CenterSelections{
		DOUBLE_SWITCH,
		SWITCH_SCALE,
		SWITCH_SCALE_COMPANION,
		SWITCH_DOUBLE_SCALE;
	}

	public boolean isStarted = false;

	private Alliance alliance;
	private RobotPosition position = RobotPosition.LEFT;

	private Command closeSwitchCloseScaleCommand;
	private Command farSwitchCloseScaleCommand;
	private Command closeSwitchFarScaleCommand;
	private Command farSwitchFarScaleCommand;
	private Command centerLeftSwitchLeftScaleCommand;
	private Command centerRightSwitchLeftScaleCommand;
	private Command centerLeftSwitchRightScaleCommand;
	private Command centerRightSwitchRightScaleCommand;
	private Command selectedAutoCommand = null;
	private CommandGroup selectedAutoCommandGroup = null;

	private DriveSelections closeSwitchCloseScaleSelection = null;
	private DriveSelections closeSwitchFarScaleSelection = null;
	private DriveSelections farSwitchCloseScaleSelection = null;
	private DriveSelections farSwitchFarScaleSelection = null;

	private CenterSelections centerLeftSwitchRightScaleSelection = null;
	private CenterSelections centerRightSwitchRightScaleSelection = null;
	private CenterSelections centerLeftSwitchLeftScaleSelection = null;
	private CenterSelections centerRightSwitchLeftScaleSelection = null;

	private ArrayList<PathCommand> selectedAutoList = new ArrayList<PathCommand>();

	private final SendableChooser<RobotPosition> positionChooser = new SendableChooser<RobotPosition>();
	private final SendableChooser<DriveSelections> closeSwitchCloseScale = new SendableChooser<DriveSelections>();
	private final SendableChooser<DriveSelections> closeSwitchFarScale = new SendableChooser<DriveSelections>();
	private final SendableChooser<DriveSelections> farSwitchCloseScale = new SendableChooser<DriveSelections>();
	private final SendableChooser<DriveSelections> farSwitchFarScale = new SendableChooser<DriveSelections>();
	private final SendableChooser<CenterSelections> centerLeftScale = new SendableChooser<CenterSelections>();
	private final SendableChooser<CenterSelections> centerRightScale = new SendableChooser<CenterSelections>();
	private final FieldData fieldData = FieldData.getInstance();

	private static final Autonomous instance = new Autonomous();
	private static Logger logger = Logger.getLogger(Autonomous.class.getSimpleName());

	/**
	 *  default constructor, this is private ensuring there is only 
	 *  one instance as defined above
	 */
	private Autonomous() {
	}

	private void setupSelectionOptions() {

		alliance = fieldData.getAlliance();

		positionChooser.addDefault("Left", RobotPosition.LEFT);
		positionChooser.addObject("Center", RobotPosition.CENTER);
		positionChooser.addObject("Right", RobotPosition.RIGHT);

		closeSwitchCloseScale.addObject(DriveSelections.DRIVE_STRAIGHT.toString(), DriveSelections.DRIVE_STRAIGHT);
		closeSwitchCloseScale.addObject(DriveSelections.SCALE.toString(), DriveSelections.SCALE);
		closeSwitchCloseScale.addDefault(DriveSelections.DOUBLE_SCALE.toString(), DriveSelections.DOUBLE_SCALE);
		closeSwitchCloseScale.addObject(DriveSelections.SCALE_SWITCH.toString(), DriveSelections.SCALE_SWITCH);

		closeSwitchFarScale.addObject(DriveSelections.DRIVE_STRAIGHT.toString(), DriveSelections.DRIVE_STRAIGHT);
		closeSwitchFarScale.addObject(DriveSelections.SCALE.toString(), DriveSelections.SCALE);
		closeSwitchFarScale.addDefault(DriveSelections.DOUBLE_SCALE.toString(), DriveSelections.DOUBLE_SCALE);
		closeSwitchFarScale.addObject(DriveSelections.SCALE_SWITCH.toString(), DriveSelections.SCALE_SWITCH);

		farSwitchCloseScale.addObject(DriveSelections.DRIVE_STRAIGHT.toString(), DriveSelections.DRIVE_STRAIGHT);
		farSwitchCloseScale.addObject(DriveSelections.SCALE.toString(), DriveSelections.SCALE);
		farSwitchCloseScale.addDefault(DriveSelections.DOUBLE_SCALE.toString(), DriveSelections.DOUBLE_SCALE);
		farSwitchCloseScale.addObject(DriveSelections.SCALE_SWITCH.toString(), DriveSelections.SCALE_SWITCH);

		farSwitchFarScale.addObject(DriveSelections.DRIVE_STRAIGHT.toString(), DriveSelections.DRIVE_STRAIGHT);
		farSwitchFarScale.addObject(DriveSelections.SCALE.toString(), DriveSelections.SCALE);
		farSwitchFarScale.addDefault(DriveSelections.DOUBLE_SCALE.toString(), DriveSelections.DOUBLE_SCALE);
		farSwitchFarScale.addObject(DriveSelections.SCALE_SWITCH.toString(), DriveSelections.SCALE_SWITCH);

		centerLeftScale.addDefault("Double Switch", CenterSelections.DOUBLE_SWITCH);
		centerLeftScale.addObject("Switch Scale", CenterSelections.SWITCH_SCALE);
		centerLeftScale.addObject("Companion Switch Scale", CenterSelections.SWITCH_SCALE_COMPANION);

		centerRightScale.addDefault("Double Switch", CenterSelections.DOUBLE_SWITCH);
		centerRightScale.addObject("Switch Scale", CenterSelections.SWITCH_SCALE);
		centerRightScale.addObject("Companion Switch Scale", CenterSelections.SWITCH_SCALE_COMPANION);

		SmartDashboard.putData("Position Chooser", positionChooser);
		SmartDashboard.putData("Close Switch Close Scale", closeSwitchCloseScale);
		SmartDashboard.putData("Close Switch Far Scale", closeSwitchFarScale);
		SmartDashboard.putData("Far Switch Close Scale", farSwitchCloseScale);
		SmartDashboard.putData("Far Switch Far Scale", farSwitchFarScale);
		SmartDashboard.putData("Center Near Scale", centerLeftScale);
		SmartDashboard.putData("Center Far Scale", centerRightScale);

		closeSwitchCloseScaleCommand = new NoOp(alliance, position);
		farSwitchCloseScaleCommand = new NoOp(alliance, position);
		closeSwitchFarScaleCommand = new NoOp(alliance, position);
		farSwitchFarScaleCommand = new NoOp(alliance, position);
		centerLeftSwitchLeftScaleCommand = new NoOp(alliance, position);
		centerRightSwitchLeftScaleCommand = new NoOp(alliance, position);
		centerLeftSwitchRightScaleCommand = new NoOp(alliance, position);
		centerRightSwitchRightScaleCommand = new NoOp(alliance, position);
	}

	public RobotPosition getSelectedPosition() {
		return positionChooser.getSelected();
	}

	/*
	 * Get methods for the four possible situations, which return the command that should be done if that position is chosen.
	 */
	public Command getCommandCloseSwitchCloseScale(){
		switch(closeSwitchCloseScaleSelection = closeSwitchCloseScale.getSelected()) {
		case DRIVE_STRAIGHT:
			return new DriveForward(alliance, position);
		case SCALE:
			return new NearScaleGroup(alliance, position);
		case DOUBLE_SCALE:
			return new NearScalePlusGroup(alliance, position);
		case SCALE_SWITCH:
			return new NearScaleNearSwitchGroup(alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandFarSwitchCloseScale() {
		switch(farSwitchCloseScaleSelection = farSwitchCloseScale.getSelected()) {
		case DRIVE_STRAIGHT:
			return new DriveForward(alliance, position);
		case SCALE:
			return new NearScaleGroup(alliance, position);
		case DOUBLE_SCALE:
			return new NearScalePlusGroup(alliance, position);
		case SCALE_SWITCH:
			return new NearScaleFarSwitchGroup(alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandFarSwitchFarScale() {
		switch(farSwitchFarScaleSelection = farSwitchFarScale.getSelected()) {
		case DRIVE_STRAIGHT:
			return new DriveForward(alliance, position);
		case SCALE:
			return new FarScaleGroup(alliance, position);
		case DOUBLE_SCALE:
			return new FarScalePlusGroup(alliance, position);
		case SCALE_SWITCH:
			return new FarScaleFarSwitchGroup(alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandCloseSwitchFarScale() {
		switch(closeSwitchFarScaleSelection = closeSwitchFarScale.getSelected()) {
		case DRIVE_STRAIGHT:
			return new DriveForward(alliance, position);
		case SCALE:
			return new FarScaleGroup(alliance, position);
		case DOUBLE_SCALE:
			return new FarScalePlusGroup(alliance, position);
		case SCALE_SWITCH:
			return new FarScaleNearSwitchGroup(alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandCenterLeftSwitchRightScale() {
		switch(centerLeftSwitchRightScaleSelection = centerRightScale.getSelected()) {
		case DOUBLE_SWITCH:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.RIGHT, Type.SWITCH, Type.SWITCH, alliance, position);
		case SWITCH_SCALE_COMPANION:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.RIGHT, Type.COMPANION_SCALE, Type.COMPANION_SCALE, alliance, position);
		case SWITCH_SCALE:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.RIGHT, Type.SCALE, Type.DOUBLE_SCALE, alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandCenterRightSwitchRightScale() {
		switch(centerRightSwitchRightScaleSelection = centerRightScale.getSelected()) {
		case DOUBLE_SWITCH:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.RIGHT, Type.SWITCH, Type.SWITCH, alliance, position);
		case SWITCH_SCALE_COMPANION:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.RIGHT, Type.COMPANION_SCALE, Type.COMPANION_SCALE, alliance, position);
		case SWITCH_SCALE:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.RIGHT, Type.SCALE, Type.DOUBLE_SCALE, alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandCenterLeftSwitchLeftScale() {
		switch(centerLeftSwitchLeftScaleSelection = centerLeftScale.getSelected()) {
		case DOUBLE_SWITCH:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.LEFT, Type.SWITCH, Type.SWITCH, alliance, position);
		case SWITCH_SCALE_COMPANION:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.LEFT, Type.COMPANION_SCALE, Type.COMPANION_SCALE, alliance, position);
		case SWITCH_SCALE:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.LEFT, Side.LEFT, Type.SCALE, Type.DOUBLE_SCALE, alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	public Command getCommandCenterRightSwitchLeftScale() {
		switch(centerRightSwitchLeftScaleSelection = centerLeftScale.getSelected()) {
		case DOUBLE_SWITCH:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.LEFT, Type.SWITCH, Type.SWITCH, alliance, position);
		case SWITCH_SCALE_COMPANION:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.LEFT, Type.COMPANION_SCALE, Type.COMPANION_SCALE, alliance, position);
		case SWITCH_SCALE:
			return new SwitchFirstThenScaleOrSwitchGroup(Side.RIGHT, Side.LEFT, Type.SCALE, Type.DOUBLE_SCALE, alliance, position);
		default:
			return new NoOp(alliance, position);
		}
	}

	/**
	 * Takes the input string from the field and turns it into a RobotPosition
	 * @param str Input string
	 * @return RobotPosition
	 */
	private RobotPosition stringToRobotPos(String str) {
		if(str.equals("L")) {
			return RobotPosition.LEFT;
		} else if(str.equals("R")) {
			return RobotPosition.RIGHT;
		} else {
			return null;
		}
	}

	/**
	 * Returns the one instance of Autonomous
	 */
	public static final Autonomous getInstance() {
		return instance;
	}

	/**
	 * This takes the selected autonomous command and start it by adding it
	 * to the scheduler.  This should be called by Robot in autonomousInit().
	 */
	public final void start() {
		selectedAutoCommand = null;
		selectedAutoCommandGroup = null;
		position = getSelectedPosition();
		alliance = fieldData.getAlliance();

		RobotPosition switchPosition = stringToRobotPos(fieldData.getFieldColorConfig(Field_Position.ALLIANCE_SWITCH));
		RobotPosition scalePosition = stringToRobotPos(fieldData.getFieldColorConfig(Field_Position.SCALE));
		RobotPosition currentPosition = positionChooser.getSelected();

		selectedAutoList.clear();
		fieldData.clearPathDisplay();

		Lift.getInstance().setShift(LiftGear.HIGH);

		if(!(switchPosition == null) || !(scalePosition == null)) {

			//Checks if the robot is in the center. If it is, don't bother with the other checks.
			if(position == RobotPosition.CENTER) {
				if(switchPosition == RobotPosition.LEFT) {
					if(scalePosition == RobotPosition.LEFT) {
						selectedAutoCommand = getCommandCenterLeftSwitchLeftScale();
					} else if(scalePosition == RobotPosition.RIGHT) {
						selectedAutoCommand = getCommandCenterLeftSwitchRightScale();
					}
				} else if(switchPosition == RobotPosition.RIGHT) {
					if(scalePosition == RobotPosition.LEFT) {
						selectedAutoCommand = getCommandCenterRightSwitchLeftScale();
					} else if(scalePosition == RobotPosition.RIGHT) {
						selectedAutoCommand = getCommandCenterRightSwitchRightScale();
					}
				}

				//Check for the rest of the positions
			} else if(currentPosition == scalePosition && currentPosition == switchPosition) {
				selectedAutoCommand = getCommandCloseSwitchCloseScale();

			}else if(currentPosition != scalePosition && currentPosition == switchPosition) {
				selectedAutoCommand = getCommandCloseSwitchFarScale();

			}else if(currentPosition == scalePosition && currentPosition != switchPosition) {
				selectedAutoCommand = getCommandFarSwitchCloseScale();

			}else if(currentPosition != scalePosition && currentPosition != switchPosition) {
				selectedAutoCommand = getCommandFarSwitchFarScale();
			}

			//Add the commands to fieldData so drivers can tell which path is running
			if(selectedAutoCommand instanceof PathGroup) {
				selectedAutoList.addAll(((PathGroup) selectedAutoCommand).getPaths());
			}else if(selectedAutoCommand instanceof PathCommand) {
				selectedAutoList.add((PathCommand) selectedAutoCommand);
			}

		}


		if(selectedAutoCommand != null) {
			selectedAutoCommandGroup = new CommandGroup() {
				{
					addSequential(new RobotState.ResetRobotState(PathCommand.relfectPose(getCurrentPosition(currentPosition), currentPosition, alliance)));
					addSequential(selectedAutoCommand);
				}
			};
		}

		for(PathCommand path : selectedAutoList) {
			fieldData.addPath(path);
		}

		if (selectedAutoCommandGroup != null) {
			logger.log(Level.INFO, "Autonomoose started. Path: " + selectedAutoCommand.toString());
			selectedAutoCommandGroup.start();
			isStarted = true;
		}

	}

	public Pose2D getCurrentPosition(RobotPosition pos) {
		switch(pos) {

		case CENTER:
			return PathConstants.MIDDLE_POSE;

		case LEFT:
		case RIGHT:
			return PathConstants.RIGHT_POSE_REVERSED;

		default:
			return null;
		}
	}

	/**
	 * This takes the selected autonomous command and stops it by canceling it.
	 * This should be called by Robot in teleopInit().
	 */
	public final void stop() {
		if (selectedAutoCommandGroup != null) {
			if (selectedAutoCommandGroup.isRunning()) {
				selectedAutoCommandGroup.cancel();
			}
		}
	}

	@Override
	public void init() {
		setupSelectionOptions();
	}

	@Override
	public void update() {

		//Check to see if any of the selections have changed.
		if(DriverStation.getInstance().isDisabled() && (position != getSelectedPosition() || alliance != fieldData.getAlliance() 
				|| closeSwitchCloseScaleSelection != closeSwitchCloseScale.getSelected() 
				|| closeSwitchFarScaleSelection != closeSwitchFarScale.getSelected() 
				|| farSwitchCloseScaleSelection != farSwitchCloseScale.getSelected() 
				|| farSwitchFarScaleSelection != farSwitchFarScale.getSelected()
				|| centerLeftSwitchLeftScaleSelection != centerLeftScale.getSelected()
				|| centerLeftSwitchRightScaleSelection != centerRightScale.getSelected()
				|| centerRightSwitchLeftScaleSelection != centerLeftScale.getSelected()
				|| centerRightSwitchRightScaleSelection != centerRightScale.getSelected())) {

			//Get latest position/alliance
			position = getSelectedPosition();
			alliance = fieldData.getAlliance();

			//Clear lists that hold the paths being displayed.
			fieldData.clearPathDisplay();
			selectedAutoList.clear();

			//Get latest commands based off of latest position/alliance and selections
			closeSwitchCloseScaleCommand = getCommandCloseSwitchCloseScale();
			closeSwitchFarScaleCommand = getCommandCloseSwitchFarScale();
			farSwitchCloseScaleCommand = getCommandFarSwitchCloseScale();
			farSwitchFarScaleCommand = getCommandFarSwitchFarScale();
			centerLeftSwitchLeftScaleCommand = getCommandCenterLeftSwitchLeftScale();
			centerRightSwitchLeftScaleCommand = getCommandCenterRightSwitchLeftScale();
			centerLeftSwitchRightScaleCommand = getCommandCenterLeftSwitchRightScale();
			centerRightSwitchRightScaleCommand = getCommandCenterRightSwitchRightScale();

			//Puts the paths into fieldData to be displayed on the extension.
			if(position == RobotPosition.CENTER) {
				if(centerLeftSwitchLeftScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) centerLeftSwitchLeftScaleCommand).getPaths());
				} else {
					selectedAutoList.add((PathCommand) centerLeftSwitchLeftScaleCommand);
				}

				if(centerRightSwitchLeftScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) centerRightSwitchLeftScaleCommand).getPaths());
				} else {
					selectedAutoList.add((PathCommand) centerRightSwitchLeftScaleCommand);
				}

				if(centerLeftSwitchRightScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) centerLeftSwitchRightScaleCommand).getPaths());
				} else {
					selectedAutoList.add((PathCommand) centerLeftSwitchRightScaleCommand);
				}

				if(centerRightSwitchRightScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) centerRightSwitchRightScaleCommand).getPaths());
				} else {
					selectedAutoList.add((PathCommand) centerRightSwitchRightScaleCommand);
				}

			} else {	
				if(closeSwitchCloseScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) closeSwitchCloseScaleCommand).getPaths());
				} else if(closeSwitchCloseScaleCommand instanceof PathCommand) {
					selectedAutoList.add((PathCommand) closeSwitchCloseScaleCommand);
				}

				if(closeSwitchFarScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) closeSwitchFarScaleCommand).getPaths());
				} else if(closeSwitchFarScaleCommand instanceof PathCommand) {
					selectedAutoList.add((PathCommand) closeSwitchFarScaleCommand);
				}

				if(farSwitchCloseScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) farSwitchCloseScaleCommand).getPaths());
				} else if(farSwitchCloseScaleCommand instanceof PathCommand) {
					selectedAutoList.add((PathCommand) farSwitchCloseScaleCommand);
				}

				if(farSwitchFarScaleCommand instanceof PathGroup) {
					selectedAutoList.addAll(((PathGroup) farSwitchFarScaleCommand).getPaths());
				} else if(farSwitchFarScaleCommand instanceof PathCommand) {
					selectedAutoList.add((PathCommand) farSwitchFarScaleCommand);
				}


			}

			for(PathCommand path : selectedAutoList) {
				fieldData.addPath(path);
			}
		}
	}
}
